package com.yoavst.server.ohelshem.notifications.controller

import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneOffset
import java.text.SimpleDateFormat
import java.util.*

object HolidayController {
    fun isHoliday(date: LocalDate): Boolean {
        val epoch = date.atStartOfDay().toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()
        return Holidays.any { epoch in it }
    }

    private val Holidays = arrayOf(
            Holiday("ראש השנה", "02/10/2016", "04/10/2016"),
            Holiday("יום כיפור", "11/10/2016", "12/10/2016"),
            Holiday("סוכות", "16/10/2016", "25/10/2016"),
            Holiday("חנוכה", "26/12/2016", "01/01/2017"),
            Holiday("פורים", "12/03/2017", "13/03/2017"),
            Holiday("פסח", "02/04/2017", "18/04/2017"),
            Holiday("יום העצמאות", "02/05/2017"),
            Holiday("ל\"ג בעומר", "14/05/2017"),
            Holiday("שבועות", "30/05/2017", "01/06/2017"))

    data class Holiday(val name: String, val start: String, val end: String = "") {
        val startTime = start.parseDate()
        val endTime = if (end.isEmpty()) startTime else end.parseDate()

        operator fun contains(time: Long): Boolean {
            return  startTime <= time && endTime >= time
        }

        companion object {
            val Format = SimpleDateFormat("dd/MM/yyyy")

            private fun String.parseDate(): Long = Holiday.Companion.Format.parse(this).time

            private fun Long.toCalendar(): Calendar {
                val cal = Calendar.getInstance()
                cal.timeInMillis = this
                return cal
            }

            /**
             * Returns the given date with the time values cleared.
             */
            private fun Calendar.clearTime(): Calendar {
                this.set(Calendar.HOUR_OF_DAY, 0)
                this.set(Calendar.MINUTE, 0)
                this.set(Calendar.SECOND, 0)
                this.set(Calendar.MILLISECOND, 0)
                return this
            }

            private fun daysBetween(day1: Calendar, day2: Calendar): Int {
                var dayOne = day1.clone() as Calendar
                var dayTwo = day2.clone() as Calendar

                if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
                    return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR))
                } else {
                    if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
                        //swap them
                        val temp = dayOne
                        dayOne = dayTwo
                        dayTwo = temp
                    }
                    var extraDays = 0

                    val dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR)

                    while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                        dayOne.add(Calendar.YEAR, -1)
                        // getActualMaximum() important for leap years
                        extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR)
                    }

                    return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays
                }
            }
        }
    }

}