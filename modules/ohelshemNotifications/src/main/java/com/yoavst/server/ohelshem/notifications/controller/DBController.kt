package com.yoavst.server.ohelshem.notifications.controller

import com.yoavst.server.ohelshem.notifications.model.Change

interface DBController {
    var changes: Array<Array<Change>>
    var changesRaw: String
}