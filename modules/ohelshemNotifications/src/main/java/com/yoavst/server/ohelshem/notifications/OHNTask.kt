package com.yoavst.server.ohelshem.notifications

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.yoavst.server.Option
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.ohelshem.notifications.controller.*
import com.yoavst.server.ohelshem.notifications.model.Change
import com.yoavst.server.ohelshem.notifications.model.UserData
import org.threeten.bp.Duration
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Period
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

@Suppress("unused")
class OHNTask : Task(
        id = "ohn",
        name = "Ohel Shem Notifications for Telegram",
        description = "A telegram bot which sends notification for changes"
) {
    override fun invoke(): TaskResults {
        val userId = query(Id)
        val userPassword = query(Password)
        val userClass = query(UserClass)
        val userLayer = query(UserLayer)
        val telegramApiKey = query(TelegramApiKey)
        val telegramChatId = query(TelegramChatId)
        val telegramLayerChatId = query(TelegramLayerChatId)

        require(userId != Id.defaultValue) { "User ID is not available" }
        require(userPassword != Password.defaultValue) { "User Password is not available" }
        require(userClass != UserClass.defaultValue) { "User Class is not available" }
        require(userLayer != UserLayer.defaultValue) { "User Layer is not available" }
        require(telegramApiKey != TelegramApiKey.defaultValue) { "Telegram Api key is not available" }
        require(telegramChatId != TelegramChatId.defaultValue) { "Telegram chat Id is not available" }
        require(telegramLayerChatId != TelegramLayerChatId.defaultValue) { "Telegram layer chat Id is not available" }

        if (shouldNotify()) {
            requestsController.userData = UserData(userId, userPassword)

            val date = timeForChanges()

            val changes = requestsController.getChanges(userLayer, day = date.dayOfMonth, month = date.monthValue, year = date.year)
            if (changes != null) {
                logger.info { "Received changes for date ${date.format(DateTimeFormatter.ISO_LOCAL_DATE)}" }
                if (changes.isEmpty()) {
                    logger.info { "Yet, Changes are empty." }
                    return TaskResults.success()
                } else {
                    val resultA = sendTelegramForSpecifiedClass(telegramApiKey, telegramChatId, databaseController.changes, changes, userClass, date.dayOfMonth, date.monthValue, date.year)
                    val resultB = sendTelegramForLayer(telegramApiKey, telegramLayerChatId, databaseController.changes, changes, userLayer, date.dayOfMonth, date.monthValue, date.year)

                    databaseController.changes = changes

                    if (resultA && resultB) {
                        logger.info { "Successfully evaluated changes for date ${date.format(DateTimeFormatter.ISO_LOCAL_DATE)}" }
                        return TaskResults.success()
                    } else {
                        logger.info { "Failed to evaluate changes for date ${date.format(DateTimeFormatter.ISO_LOCAL_DATE)}" }
                        return TaskResults.retry(Duration.ofMinutes(1))
                    }
                }
            } else {
                logger.info { "Failed to receive changes. Quiting" }
                return TaskResults.retry(Duration.ofMinutes(1))
            }
        } else {
            logger.info { "Should not notify. Quit" }
            return TaskResults.success()
        }
    }

    val requestsController: RequestsController = RequestsControllerImpl(this)
    val databaseController: DBController = DBControllerImpl(this)

    fun timeForChanges(): LocalDateTime = LocalDateTime.now().let { if (it.hour < 17) it else it + Period.ofDays(1) }

    fun shouldNotify() = LocalDateTime.now().hour >= 17 && !HolidayController.isHoliday(LocalDate.now().plusDays(1))

    //region Sending Code
    fun sendTelegramForSpecifiedClass(telegramApiKey: String, telegramChatId: String, old: Array<Array<Change>>, changes: Array<Array<Change>>,
                                      clazz: Int, day: Int, month: Int, year: Int): Boolean {
        if (old.isEmpty() || old.flatMap { it.toList() } != changes.flatMap { it.toList() }) {
            logger.info { "There is a difference between old and new changes." }
            if ((old.isEmpty() && changes.isNotEmpty()) ||
                    (changes.isEmpty() && old.isNotEmpty()) ||
                    (changes[clazz - 1].any(Change::isChanged) && !Arrays.equals(old[clazz - 1], changes[clazz - 1]))) {
                logger.info { "There are changes for the chosen class." }
                val text = toString(changes[clazz - 1], day, month, year)
                val address = "https://api.telegram.org/bot$telegramApiKey/sendMessage?chat_id=$telegramChatId&parse_mode=markdown" +
                        "&disable_web_page_preview=1&text=$text"
                logger.info { "Sending changes to telegram for $day/$month/$year" }

                val result = address.httpGet().responseString().third
                return when (result) {
                    is Result.Success -> true
                    is Result.Failure -> {
                        logger.error(result.error) { "Connection to Telegram Error" }
                        false
                    }
                }
            }
        }
        return true
    }

    fun sendTelegramForLayer(telegramApiKey: String, telegramChatId: String, old: Array<Array<Change>>, changes: Array<Array<Change>>,
                             layer: Int, day: Int, month: Int, year: Int): Boolean {
        if (old.isEmpty() || old.flatMap { it.toList() } != changes.flatMap { it.toList() }) {
            logger.info { "There is a difference between old and new changes." }
            val text = toStringLink(changes, layer, day, month, year)
            val address = "https://api.telegram.org/bot$telegramApiKey/sendMessage?chat_id=$telegramChatId&parse_mode=markdown" +
                    "&disable_web_page_preview=1&text=$text"
            logger.info { "Sending changes to telegram for $day/$month/$year" }

            val result = address.httpGet().responseString().third
            return when (result) {
                is Result.Success -> true
                is Result.Failure -> {
                    logger.error(result.error) { "Connection to Telegram Error" }
                    false
                }
            }
        }
        return true
    }

    fun toString(changes: Array<Change>, day: Int, month: Int, year: Int): String {
        return buildString {
            appendln()
            appendln("שינויים ליום $day/$month/$year ${LocalDateTime.now().let { it.hour.toString() + ":" + it.minute.two() }}".bold())
            if (changes.isEmpty() || changes.all { !it.isChanged() }) {
                appendln("אין שינויים".bold())
            } else {
                changes.forEachIndexed { i, change ->
                    if (change.isChanged())
                        appendln(((i + 1).toString() + ". " + change.text).bold())
                    else
                        appendln((i + 1).toString() + ". ")
                }
            }
        }
    }

    fun toStringLink(changes: Array<Array<Change>>, layer: Int, day: Int, month: Int, year: Int): String {
        return buildString {
            appendln()
            appendln("שינויים ליום $day/$month/$year ${LocalDateTime.now().let { it.hour.toString() + ":" + it.minute.two() }}".bold())
            if (changes.isEmpty() || changes.all { it.all { !it.isChanged() } }) {
                appendln("אין שינויים".bold())
            }
            appendln("http://ohel-shem.com/portal6/system/internal/changes.php?layer=$layer$$day$$month$$year")
        }
    }
    //endregion


    override val settings: List<Option<*>> = listOf(Id, Password, UserLayer, UserClass, TelegramApiKey, TelegramChatId, TelegramLayerChatId)

    companion object {
        private val Id = Option.Str(
                id = "id",
                name = "User ID",
                description = "User's ID to use for login",
                isProtected = true
        )

        private val Password = Option.Str(
                id = "password",
                name = "User Password",
                description = "User's password to use for login",
                isProtected = true
        )

        private val UserClass = Option.Integer(
                id = "class",
                name = "User Class",
                description = "User's Class for notifications"
        )

        private val UserLayer = Option.Integer(
                id = "layer",
                name = "User Layer",
                description = "User's Layer for notifications"
        )

        private val TelegramApiKey = Option.Str(
                id = "telegram_api_key",
                name = "Telegram Api Key",
                description = "The Telegram Api key for bot"
        )

        private val TelegramChatId = Option.Str(
                id = "telegram_chat_id",
                name = "Telegram Channel Id",
                description = "The Telegram Channel id for the bot"
        )
        private val TelegramLayerChatId = Option.Str(
                id = "telegram_layer_chat_id",
                name = "Telegram Channel Id for Layer Info",
                description = "The Telegram Channel id for the bot for layer information"
        )
    }
}