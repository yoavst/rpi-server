package com.yoavst.server.ohelshem.notifications.controller

import com.yoavst.server.ohelshem.notifications.model.Change
import com.yoavst.server.ohelshem.notifications.model.UserData

interface RequestsController {
    var userData: UserData

    fun getChanges(layer: Int, day: Int, month: Int, year: Int): Array<Array<Change>>?
}