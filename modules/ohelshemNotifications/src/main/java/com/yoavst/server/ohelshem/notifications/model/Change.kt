package com.yoavst.server.ohelshem.notifications.model

data class Change(val text: String = "") {
    fun isChanged(): Boolean = text.length != 0
}