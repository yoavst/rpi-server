package com.yoavst.server.ohelshem.notifications

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.text.substring

inline fun <reified T : Any> typeToken(): Type = object : TypeToken<T>() {}.type

val gson = Gson()

fun String.toColor(): Int {
    // Use a long to avoid rollovers on #ffXXXXXX
    var color = java.lang.Long.parseLong(substring(1), 16)
    if (length == 7) {
        // Set the alpha value
        color = color or -16777216
    } else if (length != 9) {
        throw IllegalArgumentException("Unknown color $this")
    }
    return color.toInt()
}

fun Int.two() = if (this < 10) "0" + this else this.toString()

fun String.bold(): String {
    return "*$this*"
}
