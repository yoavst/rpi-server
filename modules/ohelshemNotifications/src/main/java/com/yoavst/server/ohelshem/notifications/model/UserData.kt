package com.yoavst.server.ohelshem.notifications.model

data class UserData(val id: String, val password: String)