package com.yoavst.server.ohelshem.notifications.controller

import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.yoavst.server.Task
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.ohelshem.notifications.model.Change
import com.yoavst.server.ohelshem.notifications.model.UserData
import com.yoavst.server.ohelshem.notifications.two
import com.yoavst.server.ohelshem.notifications.typeToken

class RequestsControllerImpl(task: Task) : RequestsController, Logging(task, "Ohelshem Notifications Requests") {
    override lateinit var userData: UserData

    override fun getChanges(layer: Int, day: Int, month: Int, year: Int): Array<Array<Change>>? {
        val result = addr.httpPost().body(
                "identity=${userData.id}&password=${userData.password}" +
                        "&date=$year-${month.two()}-${day.two()}" +
                        "&layer=$layer"
        ).responseString().third

        return when (result) {
            is Result.Success -> result.value.parse()
            is Result.Failure -> {
                logger.error(result.error) {"Connection to Ohel Shem Error" }
                null
            }
        }
    }

    private val addr = "https://ohel-shem.com/portal6/system/mobile_api_v1/layerchanges.php"

    internal fun String.parse(): Array<Array<Change>> {
        val changes = Gson().fromJson<Array<ApiChange>>(this, typeToken<Array<ApiChange>>()).filterNot {
            it.content.isBlank()
        }
        if (changes.isEmpty()) return emptyArray()
        return Array(12) { c ->
            val data = changes.filter { it.clazz == c + 1 }
            Array(10) { h ->
                (data.firstOrNull { it.hour == h + 1 })?.let { Change(it.content) } ?: Change()
            }
        }

    }

    data class ApiChange(@SerializedName("class") val clazz: Int, val hour: Int, val content: String)
}

