package com.yoavst.server.ohelshem.notifications.controller

import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.ohelshem.notifications.gson
import com.yoavst.server.ohelshem.notifications.model.Change
import com.yoavst.server.ohelshem.notifications.typeToken
import com.yoavst.server.ohelshem.notifications.controller.DBController
import java.io.File

class DBControllerImpl(task: Task) : DBController {
    override var changes: Array<Array<Change>>
        get() = changesRaw.let {
            if (it.isEmpty()) emptyArray<Array<Change>>()
            else gson.fromJson(it, typeToken<Array<Array<Change>>>()) }
        set(value) {
            changesRaw = gson.toJson(value)
        }
    override var changesRaw: String
        get() = CacheFile.readText()
        set(value) {
            CacheFile.writeText(value)
        }

    private val CacheFile by lazy {
        Environment.storage.file(task, "ohelshem.json")
    }
}