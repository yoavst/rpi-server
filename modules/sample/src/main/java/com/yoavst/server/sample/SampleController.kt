package com.yoavst.server.sample

import com.yoavst.server.Task
import com.yoavst.server.logging.impl.Logging


class SampleController(task: Task): Logging(task, "Sample Controller") {
    fun sayHi() {
        logger.info(Exception("Normal is boring!")) { "Hi Everybody!" }
    }
}