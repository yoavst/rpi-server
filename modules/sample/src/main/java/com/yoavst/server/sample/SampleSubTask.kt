package com.yoavst.server.sample

import com.yoavst.server.SubTask
import com.yoavst.server.TaskResults

class SampleSubTask(task: SampleTask) : SubTask(
        task,
        id = "tstSub",
        name = "Sample Subtask",
        description = "A sample subtask that does nothing"
) {
    override fun invoke(): TaskResults {
        logger.info { "Hello from sample subtask!" }
        return TaskResults.success()
    }
}