package com.yoavst.server.sample

import com.yoavst.server.SubTask
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import org.threeten.bp.Duration

@Suppress("unused")
class SampleTask : Task(
        id = "tst",
        name = "Sample Task",
        description = "A sample task that does nothing"
) {
    override fun invoke(): TaskResults {
        logger.info { "Hello from sample task!" }

        val controller = SampleController(this)
        controller.sayHi()

        val random = Math.random()
        logger.info { "The random result was $random" }


        return if (random > 0.5) {
            TaskResults.retry(Duration.ofSeconds(10))
        } else TaskResults.success()
    }

    override val subTasks: List<SubTask> = listOf(SampleSubTask(this))
}