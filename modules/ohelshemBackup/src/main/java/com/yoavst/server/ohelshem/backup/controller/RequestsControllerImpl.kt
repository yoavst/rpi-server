package com.yoavst.server.ohelshem.backup.controller

import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.JsonParser
import com.yoavst.server.Task
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.ohelshem.backup.model.UserData

class RequestsControllerImpl(task: Task) : RequestsController, Logging(task, "Ohelshem Backup Requests") {
    override lateinit var userData: UserData

    override fun getBackup(): String? {
        val result = addr.httpPost().body("identity=${userData.id}&password=${userData.password}").responseString().third
        return when (result) {
            is Result.Success -> {
                val code = jsonParser.parse(result.value).asJsonObject.getAsJsonObject("error").get("code").asInt
                if (code == 0)
                    result.value
                else null
            }
            is Result.Failure -> {
                logger.error(result.error) {"Connection to Ohel Shem Error" }
                null
            }
        }
    }

    private val addr = "https://ohel-shem.com/portal6/system/mobile_api_v2/backup.php"
    private val jsonParser = JsonParser()

}