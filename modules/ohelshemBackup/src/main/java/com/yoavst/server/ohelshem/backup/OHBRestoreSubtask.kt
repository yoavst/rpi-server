package com.yoavst.server.ohelshem.backup

import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.yoavst.server.Environment
import com.yoavst.server.SubTask
import com.yoavst.server.TaskResults
import java.io.File

class OHBRestoreSubtask(task: OHBTask) : SubTask(
        task,
        id = "ohbr",
        name = "Ohel Shem Restore",
        description = "Restore Ohelshem timetable"
) {
    override fun invoke(): TaskResults {
        val folder = File(Environment.storage.folder(task), OHBTask.BackupFolder)
        val lastBackup = folder.listFiles().filterNot { it.name.startsWith(OHBSkipperSubtask.SkipPrefix) }.maxBy(File::lastModified) ?: run {
            logger.info { "No available backup file. Aborting." }
            return TaskResults.failure()
        }

        logger.info { "Loading from backup file: ${lastBackup.nameWithoutExtension}" }

        val json = Gson().toJson(JsonParser().parse(lastBackup.reader()).asJsonObject.getAsJsonArray("timetable"))

        logger.info { "Upload backup to web server" }

        val address = "https://ohel-shem.com/portal6/system/mobile_api_v2/timetable_restore.php?identity=${query(OHBTask.Id)}&password=${query(OHBTask.Password)}"

        val result = address.httpPost().body(json).responseString().third
        return when (result) {
            is Result.Success -> {
                folder.listFiles().filter { it.name.startsWith(OHBSkipperSubtask.SkipPrefix) }.forEach {
                    it.renameTo(File(it.parentFile, it.name.removePrefix(OHBSkipperSubtask.SkipPrefix)))
                }
                TaskResults.success()
            }
            is Result.Failure -> {
                logger.error(result.error) { "Connection to school web server failed" }
                TaskResults.failure()
            }
        }
    }
}