package com.yoavst.server.ohelshem.backup.controller

import com.yoavst.server.ohelshem.backup.model.UserData

interface RequestsController {
    var userData: UserData

    fun getBackup(): String?
}