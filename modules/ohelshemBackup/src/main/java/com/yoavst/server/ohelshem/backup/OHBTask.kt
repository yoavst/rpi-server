package com.yoavst.server.ohelshem.backup

import com.yoavst.server.*
import com.yoavst.server.ohelshem.backup.controller.RequestsController
import com.yoavst.server.ohelshem.backup.controller.RequestsControllerImpl
import com.yoavst.server.ohelshem.backup.model.UserData
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import java.io.File

@Suppress("unused")
class OHBTask : Task(
        id = "ohb",
        name = "Ohel Shem Backup",
        description = "Backup for Ohelshem's important data"
) {
    override fun invoke(): TaskResults {
        val userId = query(Id)
        val userPassword = query(Password)

        require(userId != Id.defaultValue) { "User ID is not available" }
        require(userPassword != Password.defaultValue) { "User Password is not available" }

        requestsController.userData = UserData(userId, userPassword)

        val data = requestsController.getBackup()
        if (data == null) {
            logger.info { "Failed to receive data from server." }
            return TaskResults.retry(Duration.ofMinutes(1))
        } else {
            logger.info { "Successfully received data from server. Backup." }
            val now = LocalDateTime.now()
            val filename = "${now.dayOfMonth}_${now.monthValue}_${now.year}__${now.hour}_${now.minute}.json"
            val dir = File(Environment.storage.folder(this), BackupFolder).apply { mkdir() }
            File(dir, filename).writeText(data)
            return TaskResults.success()
        }
    }

    val requestsController: RequestsController = RequestsControllerImpl(this)


    override val settings: List<Option<*>> = listOf(Id, Password)
    override val subTasks: List<SubTask> = listOf(OHBRestoreSubtask(this), OHBSkipperSubtask(this))

    companion object {
        internal const val BackupFolder = "backup"
        internal val Id = Option.Str(
                id = "id",
                name = "User ID",
                description = "User's ID to use for login",
                isProtected = true
        )

        internal val Password = Option.Str(
                id = "password",
                name = "User Password",
                description = "User's password to use for login",
                isProtected = true
        )
    }
}