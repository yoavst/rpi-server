package com.yoavst.server.ohelshem.backup

import com.yoavst.server.Environment
import com.yoavst.server.SubTask
import com.yoavst.server.TaskResults
import java.io.File

class OHBSkipperSubtask(task: OHBTask) : SubTask(
        task,
        id = "ohbrs",
        name = "Ohel Shem restore Skipper",
        description = "Skip last backup for restoring"
) {
    override fun invoke(): TaskResults {
        val folder = File(Environment.storage.folder(task), OHBTask.BackupFolder)
        val lastBackup = folder.listFiles().filterNot { it.name.startsWith(SkipPrefix) }.maxBy(File::lastModified) ?: return TaskResults.success()

        logger.info { "Skipping backup file: ${lastBackup.nameWithoutExtension}" }

        lastBackup.renameTo(File(lastBackup.parentFile, SkipPrefix + lastBackup.name))

        return TaskResults.success()


    }


    companion object {
        val SkipPrefix = "_skip_"
    }
}