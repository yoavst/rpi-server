package com.yoavst.server.ohelshem.backup.model

data class UserData(val id: String, val password: String)