package com.yoavst.server.aman.notifications.model

data class Questionnaire(val name: String, val instructions: String, val lastDate: Long, val status: String)