package com.yoavst.server.aman.notifications.controller

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.JsonParser
import com.yoavst.server.Task
import com.yoavst.server.aman.notifications.controller.RequestsController.Companion.IdError
import com.yoavst.server.aman.notifications.controller.RequestsController.Companion.NetworkError
import com.yoavst.server.aman.notifications.controller.RequestsController.Companion.PasswordError
import com.yoavst.server.aman.notifications.model.*
import com.yoavst.server.logging.impl.Logging
import org.jsoup.Jsoup
import java.net.CookieManager
import java.net.URI
import java.nio.charset.Charset
import java.text.SimpleDateFormat

@Suppress("UNUSED_VARIABLE")
class RequestsControllerImpl(task: Task) : RequestsController, Logging(task, "Aman Notifications Requests") {
    private val cookieManager = CookieManager()

    override lateinit var userData: UserData

    override fun login(): Int? {
        val (request, response, result) = AuthUrl.httpPost()
                .header("Content-type" to "application/json; charset=UTF-8")
                .body("""{misparMoamad: "${userData.id}", userPass: "${userData.password}"}""")
                .responseString()
        when (result) {
            is Result.Success -> {
                val data = JsonParser().parse(result.value).asJsonObject["d"].asJsonObject
                val serverId = data["MisparMoamad"].asInt
                if (serverId == IdError || serverId == PasswordError) return serverId
                else {
                    cookieManager.put(URI(AuthUrl), response.headers)
                    return null
                }
            }
            is Result.Failure -> {
                logger.error(result.error) { "Connection to Aman Error" }
                return NetworkError
            }
        }

    }

    override fun query(): Data? {
        val (request, response, result) = DataUrl.httpGet().header("Cookie" to cookieManager.cookieStore.cookies.joinToString(";"))
                .responseObject(deserializer)
        when (result) {
            is Result.Success -> {
                cookieManager.put(URI(AuthUrl), response.headers)
                return result.value.parse()
            }
            is Result.Failure -> {
                logger.error(result.error) { "Connection to Aman data Error" }
                return null
            }
        }
    }

    private fun String.parse(): Data {
        val document = Jsoup.parse(this)

        val routes: List<Route> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSection1")?.run {
            children().drop(1).map {
                Route(
                        name = it.getElementsByClass("CourseName")[0].text(),
                        status = it.getElementsByClass("CourseStatus")[0].text()
                )
            }
        } ?: emptyList()

        val questionnaires: List<Questionnaire> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSecrion2")?.run {
            children().drop(1).map {
                Questionnaire(
                        name = it.getElementsByClass("SurveyName")[0].text(),
                        instructions = it.getElementsByClass("SurveyInstructions")[0].text(),
                        lastDate = DateParser.parse(it.getElementsByClass("SurveyLastDate")[0].text()).time,
                        status = it.getElementsByClass("SurveyStatus")[0].text()
                )
            }
        } ?: emptyList()

        val events: List<Event> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSection3")?.run {
            getElementsByClass("TableRow").map {
                Event(
                        name = it.getElementsByClass("EventName")[0].text(),
                        location = it.getElementsByClass("EventPlace")[0].text(),
                        date = FullDateParser.parse(it.getElementsByClass("EventDate")[0].text()).time,
                        hasApproved = it.children().last().text().contains("אושר")
                )
            }
        } ?: emptyList()

        return Data(routes, questionnaires, events)
    }

    private val DateParser = SimpleDateFormat("dd/MM/yyyy")
    private val FullDateParser = SimpleDateFormat("dd/MM/yyyy ב-HH:mm")
    private val AuthUrl = "https://www.aman.idf.il/Modiin/AuthenticationService.asmx/Authenticate"
    private val DataUrl = "https://www.aman.idf.il/modiin/Kiosk.aspx?catId=59951"

    private val deserializer = object : ResponseDeserializable<String> {
        override fun deserialize(bytes: ByteArray): String {
            return String(bytes, Charset.forName("windows-1255"))
        }
    }
}