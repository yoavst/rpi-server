package com.yoavst.server.aman.notifications.model

data class Route(val name: String, val status: String)