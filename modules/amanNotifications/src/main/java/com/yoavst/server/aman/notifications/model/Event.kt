package com.yoavst.server.aman.notifications.model

data class Event(val name: String, val location: String, val date: Long, val hasApproved: Boolean)