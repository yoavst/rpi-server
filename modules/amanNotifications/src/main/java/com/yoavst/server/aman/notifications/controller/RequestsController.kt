package com.yoavst.server.aman.notifications.controller

import com.yoavst.server.aman.notifications.model.Data
import com.yoavst.server.aman.notifications.model.UserData

interface RequestsController {
    var userData: UserData

    fun login(): Int?
    fun query(): Data?

    companion object {
        const val NetworkError = 0
        const val IdError = -1
        const val PasswordError = -2
    }
}