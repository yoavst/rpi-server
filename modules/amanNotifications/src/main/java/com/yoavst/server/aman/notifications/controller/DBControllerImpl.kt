package com.yoavst.server.aman.notifications.controller

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.aman.notifications.model.Data
import java.io.File

class DBControllerImpl(task: Task) : DBController {
    override var data: Data?
        get() {
            val text = CacheFile.readText()
            if (text.isEmpty()) return null
            else return gson.fromJson(CacheFile.reader(Charsets.UTF_8))
        }
        set(value) {
            if (value == null)
                CacheFile.writeText("")
            else
                CacheFile.writeText(gson.toJson(value))

        }

    private val gson = Gson()

    private val CacheFile by lazy {
        Environment.storage.file(task, "aman_notifications.json")
    }
}