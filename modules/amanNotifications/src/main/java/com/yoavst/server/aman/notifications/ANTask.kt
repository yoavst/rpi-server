package com.yoavst.server.aman.notifications

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.yoavst.server.Option
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.aman.notifications.controller.DBController
import com.yoavst.server.aman.notifications.controller.DBControllerImpl
import com.yoavst.server.aman.notifications.controller.RequestsController
import com.yoavst.server.aman.notifications.controller.RequestsControllerImpl
import com.yoavst.server.aman.notifications.model.Data
import com.yoavst.server.aman.notifications.model.UserData
import org.threeten.bp.Duration
import java.text.SimpleDateFormat
import java.util.*

@Suppress("unused")
class ANTask : Task(
        id = "an",
        name = "Aman Notifications for Telegram",
        description = "A telegram bot which sends notification for aman updates"
) {
    override fun invoke(): TaskResults {
        val userId = query(Id)
        val userPassword = query(Password)
        val telegramApiKey = query(TelegramApiKey)
        val telegramChatId = query(TelegramChatId)

        require(userId != Id.defaultValue) { "User ID is not available" }
        require(userPassword != Password.defaultValue) { "User Password is not available" }
        require(telegramApiKey != TelegramApiKey.defaultValue) { "Telegram Api key is not available" }
        require(telegramChatId != TelegramChatId.defaultValue) { "Telegram chat Id is not available" }

        requestsController.userData = UserData(userId, userPassword)

        val loginResult = requestsController.login()
        if (loginResult == null) {
            return query(telegramApiKey, telegramChatId)
        } else {
            logger.info { "Failed to login, trying again after a minute." }
            Thread.sleep(60000)
            val loginResult2 = requestsController.login()
            if (loginResult2 == null) {
                return query(telegramApiKey, telegramChatId)
            } else {
                logger.error { "Failed to login again." }
                return TaskResults.failure() // Don't want to abuse the system.
            }
        }
    }

    private fun query(telegramApiKey: String, telegramChatId: String): TaskResults {
        logger.info { "Successfully login to Aman service." }
        val data = requestsController.query()
        if (data == null) {
            logger.info { "Failed to query, trying again." }
            Thread.sleep(60000)
            val data2 = requestsController.query()
            if (data2 == null) {
                logger.error { "Failed to query data again." }
                return TaskResults.failure() // Don't want to abuse the system.
            } else {
                return process(telegramApiKey, telegramChatId, data2)
            }
        } else {
            return process(telegramApiKey, telegramChatId, data)
        }
    }

    private fun process(telegramApiKey: String, telegramChatId: String, data: Data): TaskResults {
        logger.info { "Successfully received data from aman service." }
        val old = databaseController.data
        val result = notify(telegramApiKey, telegramChatId, old, data)
        databaseController.data = data

        if (result) {
            logger.info { "Successfully evaluated aman data." }
            return TaskResults.success()
        } else {
            logger.info { "Failed to evaluate aman data." }
            return TaskResults.retry(Duration.ofMinutes(1))
        }
    }

    private fun notify(telegramApiKey: String, telegramChatId: String, old: Data?, current: Data): Boolean {
        if (old != current) {
            logger.info { "There was a difference between old and current data from aman." }
            val text = toString(diff(old, current))
            val address = "https://api.telegram.org/bot$telegramApiKey/sendMessage?chat_id=$telegramChatId&parse_mode=markdown&disable_web_page_preview=1&text=$text"

            val result = address.httpGet().responseString().third
            return when (result) {
                is Result.Success -> true
                is Result.Failure -> {
                    logger.error(result.error) { "Connection to Telegram Error" }
                    false
                }
            }
        } else {
            return true
        }
    }

    private fun diff(old: Data?, current: Data): Data {
        if (old == null) return current
        val routes = current.routes.filterNot { it in old.routes }
        val questionnaires = current.questionnaires.filterNot { it in old.questionnaires }
        val events = current.events.filterNot { it in old.events }
        return Data(routes, questionnaires, events)
    }

    private fun toString(data: Data): String {
        return buildString {
            appendln()
            if (data.routes.isNotEmpty()) {
                appendln("שינוי במסלולים:".bold())
                data.routes.forEach {
                    appendln(it.name + ": " + it.status)
                }
            }
            if (data.questionnaires.isNotEmpty()) {
                appendln("שינוי בשאלונים:".bold())
                data.questionnaires.forEach {
                    appendln(it.name + " - " + it.status)
                }
            }
            if (data.events.isNotEmpty()) {
                appendln("שינוי באירועים:".bold())
                data.events.forEach {
                    appendln(it.name + " " + SimpleDateFormat("dd/MM/yyyy ב-HH:mm").format(Date(it.date)))
                }
            }
        }
    }

    val requestsController: RequestsController = RequestsControllerImpl(this)
    val databaseController: DBController = DBControllerImpl(this)

    override val settings: List<Option<*>> = listOf(Id, Password, TelegramApiKey, TelegramChatId)

    companion object {
        private val Id = Option.Str(
                id = "id",
                name = "User ID",
                description = "User's ID to use for login",
                isProtected = true
        )

        private val Password = Option.Str(
                id = "password",
                name = "User Password",
                description = "User's password to use for login",
                isProtected = true
        )


        private val TelegramApiKey = Option.Str(
                id = "telegram_api_key",
                name = "Telegram Api Key",
                description = "The Telegram Api key for bot"
        )

        private val TelegramChatId = Option.Str(
                id = "telegram_chat_id",
                name = "Telegram Channel Id",
                description = "The Telegram Channel id for the bot"
        )
    }
}