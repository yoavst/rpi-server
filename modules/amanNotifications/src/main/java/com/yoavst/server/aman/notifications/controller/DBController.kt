package com.yoavst.server.aman.notifications.controller

import com.yoavst.server.aman.notifications.model.Data

interface DBController {
    var data: Data?
}