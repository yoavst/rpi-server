package com.yoavst.server.aman.notifications

fun String.bold(): String {
    return "*$this*"
}