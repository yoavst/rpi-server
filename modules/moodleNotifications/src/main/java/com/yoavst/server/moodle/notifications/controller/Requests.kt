package com.yoavst.server.moodle.notifications.controller

import com.yoavst.server.moodle.notifications.model.Content
import com.yoavst.server.moodle.notifications.model.ContentFile
import com.yoavst.server.moodle.notifications.model.Course
import java.io.File

interface Requests {
    fun generateMoodleToken(username: String, id: String, password: String): String?
    fun getUserId(token: String): String
    fun getCourses(token: String, userMoodleId: String): List<Course>
    fun getContentsFor(course: Course, token: String): List<Content>
    fun downloadResource(resource: String, token: String, file: File)
    fun getAssignmentsFiles(course: Course, token: String): Map<Int, List<ContentFile>>
}