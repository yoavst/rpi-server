package com.yoavst.server.moodle.notifications

import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpUpload
import com.github.kittinunf.result.Result
import com.yoavst.server.Environment
import com.yoavst.server.Option
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.moodle.notifications.controller.Requests
import com.yoavst.server.moodle.notifications.controller.RequestsImpl
import com.yoavst.server.moodle.notifications.controller.Storage
import com.yoavst.server.moodle.notifications.controller.StorageImpl
import com.yoavst.server.moodle.notifications.controller.mytau.TauImsScraper
import com.yoavst.server.moodle.notifications.model.Content
import com.yoavst.server.moodle.notifications.model.ContentFile
import com.yoavst.server.moodle.notifications.model.Course
import org.jsoup.Jsoup
import org.threeten.bp.Duration
import java.io.File
import java.net.URI


class MONTask : Task(
        id = "mon",
        name = "Moodle notifications for Telegram",
        description = "A Telegram bot which sends notifications for updated files"
) {
    override fun invoke(): TaskResults {
        val userId = query(Id)
        val username = query(Username)
        val userPassword = query(Password)
        val academicSemester = query(AcademicSemester)
        val academicYear = query(AcademicYear)
        val telegramApiKey = query(TelegramApiKey)
        val telegramChatId = query(TelegramChatId)


        require(userId != Id.defaultValue) { "User ID is not available" }
        require(username != Username.defaultValue) { "Username is not available" }
        require(userPassword != Password.defaultValue) { "User Password is not available" }
        require(academicYear != AcademicYear.defaultValue) { "Academic year is not available" }
        require(academicSemester != AcademicSemester.defaultValue) { "Academic Semester is not available" }
        require(telegramApiKey != TelegramApiKey.defaultValue) { "Telegram Api key is not available" }
        require(telegramChatId != TelegramChatId.defaultValue) { "Telegram chat Id is not available" }

        val notification: (String) -> Unit = { notify(telegramApiKey, telegramChatId, it) }
        val fileSending: (file: File) -> Unit = { sendFile(telegramApiKey, telegramChatId, it) }

        val loginStatus = login(username, userId, userPassword, query(MoodleToken), query(MoodleId), notification)
        if (!loginStatus)
            return TaskResults.retry(Duration.ofMinutes(1))

        val moodleToken = query(MoodleToken)
        val moodleId = query(MoodleId)

        var courses = storage.courses
        if (courses.isEmpty()) {
            try {
                val now = System.currentTimeMillis()
                courses = requests.getCourses(moodleToken, moodleId).filter { it.endDate * 1000 >= now }
            } catch (e: Exception) {
                logger.error(e) { "Failed to load courses" }
                notification("${"Error:".title()} could not load list of courses!")

                val token = requests.generateMoodleToken(username, userId, userPassword)
                if (token != null) {
                    set(MoodleToken, token)
                } else {
                    logger.error { "Failed to regenerate token" }
                }

                return TaskResults.retry(Duration.ofMinutes(1))
            }
            storage.courses = courses
        }

        for (course in courses) {
            val contents = requests.getContentsFor(course, moodleToken)
            val diff = diff(course, contents)
            storage.setContentsFor(course, contents)
            if (diff.isNotEmpty()) {
                val diffText = generateDiffText(course, diff)
                notification(diffText)
                diff.flatMap { (key, value) -> value.map { key.modIcon to it } }.forEach { (modIcon, it) ->
                    val isUrlDownload = "url" !in modIcon
                    if (it.type == ContentFile.TYPE_FILE || isUrlDownload) {
                        val tempFile = Environment.storage.file(this, if (isUrlDownload) URI(it.url).path.split('/').last() else it.name, create = true)
                        try {
                            requests.downloadResource(it.url, moodleToken, tempFile)
                            fileSending(tempFile)
                        } finally {
                            tempFile.delete()
                        }
                    } else {
                        notification(it.name + "\r\n" + it.url)
                    }
                }
                val assignments = diff.keys.filter { it.modName == "assign" }
                if (assignments.isNotEmpty()) {
                    val resources = requests.getAssignmentsFiles(course, moodleToken)
                    assignments.mapNotNull { resources[it.instance] }.forEach { files ->
                        files.forEach {
                            val tempFile = Environment.storage.file(this, it.name, create = true)
                            try {
                                requests.downloadResource(it.url, moodleToken, tempFile)
                                fileSending(tempFile)
                            } finally {
                                tempFile.delete()
                            }
                        }
                    }
                }
            }
        }

        logger.info { "Successfully received data about all courses" }
        logger.info { "Querying tests" }

        val gradesAndScansQuery = queryGradesAndScans(userId, username, userPassword, academicYear, academicSemester, notification)
        if (!gradesAndScansQuery)
            TaskResults.retry(Duration.ofMinutes(1))

        return TaskResults.success()
    }

    //region Site queries
    private fun login(username: String, userId: String, userPassword: String, moodleToken: String, moodleId: String, notification: (String) -> Unit): Boolean {
        if (moodleToken == MoodleToken.defaultValue) {
            val token = requests.generateMoodleToken(username, userId, userPassword)
            if (token == null) {
                logger.error { "Failed to generate user token, aborting." }
                notification("${"Error:".title()} could not generate user token!")
                return false
            }
            set(MoodleToken, token)
        }

        if (moodleId == MoodleId.defaultValue) {
            try {
                val newMoodleId = requests.getUserId(moodleToken)
                set(MoodleId, newMoodleId)
            } catch (e: Exception) {
                logger.error(e) { "Failed to obtain user moodle id, aborting." }
                notification("${"Error:".title()} could not obtain user moodle id!")
                return false
            }
        }
        return true
    }

    private fun queryGradesAndScans(userId: String, username: String, userPassword: String, academicYear: Int, academicSemester: Int, notification: (String) -> Unit): Boolean {
        val scrapper = TauImsScraper()
        try {
            scrapper.login(userId, username, userPassword)
        } catch (e: Exception) {
            logger.error(e) { "Failed to load courses" }
            notification("${"Error:".title()} could not login to mytau!")
            return false
        }

        var majors = storage.majors
        if (majors.isEmpty()) {
            try {
                majors = scrapper.majors
                storage.majors = majors
            } catch (e: Exception) {
                logger.error(e) { "Failed to load courses" }
                notification("${"Error:".title()} could not load list of majors from mytau!")
                return false
            }
        }
        val collectedGrades = mutableMapOf<String, String>()
        val collectedScans = mutableMapOf<String, Boolean>()
        for (major in majors) {
            collectedGrades += scrapper.getCourseGrades(academicYear, academicSemester, major)
            collectedScans += scrapper.getExamScans(academicYear, academicSemester, major)
        }
        val gradesDiff = gradesDiff(storage.grades, collectedGrades)
        if (gradesDiff.isNotEmpty()) {
            notification(generateGradesText(gradesDiff))
        }

        val scansDiff = scansDiff(storage.scans, collectedScans)
        if (scansDiff.isNotEmpty()) {
            notification(generateScansText(scansDiff))
        }

        storage.grades = collectedGrades
        storage.scans = collectedScans

        return true
    }
    //endregion

    //region Text and diff utils
    private fun diff(course: Course, contents: List<Content>): Map<Content, List<ContentFile>> {
        val originalContents = storage.contentsFor(course)
        val diff = mutableMapOf<Content, MutableList<ContentFile>>()
        for (content in contents) {
            val originalContent = originalContents.firstOrNull { it.id == content.id }
            if (originalContent != null) {
                if (content.files.isNotEmpty()) {
                    for (contentFile in content.files) {
                        if (originalContent.files.none { it.name == contentFile.name }) {
                            diff.getOrPut(content) { mutableListOf() }.add(contentFile)
                        }
                    }
                }
            } else {
                if (content.files.isNotEmpty()) {
                    for (contentFile in content.files) {
                        diff += content to content.files.toMutableList()
                    }
                } else {
                    diff += content to mutableListOf()
                }
            }
        }
        return diff
    }

    private fun gradesDiff(old: Map<String, String>, new: Map<String, String>): Map<String, String> {
        val difference = mutableMapOf<String, String>()
        for ((key, value) in new) {
            if (value.isNotBlank() && (key !in old || old[key] != value)) {
                difference += key to value
            }
        }
        return difference
    }

    private fun scansDiff(old: Map<String, Boolean>, new: Map<String, Boolean>): List<String> {
        val difference = mutableListOf<String>()
        for ((key, value) in new) {
            if (value && (key !in old || old[key]!!.not())) {
                difference += key
            }
        }
        return difference
    }

    private fun generateDiffText(course: Course, courseDiff: Map<Content, List<ContentFile>>): String = buildString {
        appendln("${course.getHebrewName()}:".title())
        appendln()
        val ordered = courseDiff.asSequence().groupBy({ it.key.categoryName }, { it.key to it.value })
        for ((category, data) in ordered) {
            appendln("${category.trimStart()}:".title())
            for ((content, files) in data) {
                appendln("${content.sectionName}:")
                if (files.isNotEmpty()) {
                    for (file in files) {
                        appendln(file.name)
                    }
                } else {
                    appendln(Jsoup.parse(content.description).text())
                }
            }
        }
    }

    private fun generateGradesText(diff: Map<String, String>): String = buildString {
        appendln("ציונים חדשים:".title())
        diff.forEach { (name, grade) -> appendln("$name: $grade") }
    }

    private fun generateScansText(diff: List<String>): String = buildString {
        appendln("סריקות חדשות:".title())
        diff.forEach { name -> appendln(name) }
    }
    //endregion

    //region Telegram utils
    private fun notify(telegramApiKey: String, telegramChatId: String, text: String): Boolean {
        logger.info { "Notifying telegram" }

        val address = "https://api.telegram.org/bot$telegramApiKey/sendMessage?chat_id=$telegramChatId&parse_mode=html&disable_web_page_preview=1&text=$text"
        val (_, _, result) = address.httpGet().responseString()
        return when (result) {
            is Result.Success -> true
            is Result.Failure -> {
                logger.error(result.error) { "Connection to Telegram Error" }
                false
            }
        }
    }

    private fun sendFile(telegramApiKey: String, telegramChatId: String, file: File): Boolean {
        val (_, _, result) = "https://api.telegram.org/bot$telegramApiKey/sendDocument?chat_id=$telegramChatId".httpUpload()
                .dataParts { _, _ -> listOf(DataPart(file, name = "document")) }.responseString()
        return when (result) {
            is Result.Success -> true
            is Result.Failure -> {
                logger.error(result.error) { "Connection to Telegram Error" }
                false
            }
        }
    }
    //endregion

    private val requests: Requests = RequestsImpl(this)
    private val storage: Storage = StorageImpl(this)
    override val settings: List<Option<*>> = listOf(Id, Username, Password, TelegramApiKey, TelegramChatId, MoodleToken)

    companion object {
        private val Id = Option.Str(
                id = "id",
                name = "User ID",
                description = "User's ID for TAU login",
                isProtected = true
        )

        private val Username = Option.Str(
                id = "username",
                name = "Username",
                description = "Username for TAU login",
                isProtected = true
        )

        private val Password = Option.Str(
                id = "password",
                name = "User Password",
                description = "User's password for TAU login",
                isProtected = true
        )

        private val AcademicYear = Option.Integer(
                id = "academicYear",
                name = "Academic year",
                description = "current Academic year"
        )

        private val AcademicSemester = Option.Integer(
                id = "academicSemester",
                name = "Academic semester",
                description = "current Academic semester"
        )

        private val MoodleToken = Option.Str(
                id = "moodle_token",
                name = "Moodle token",
                description = "Moodle auth token",
                isProtected = true
        )

        private val MoodleId = Option.Str(
                id = "moodle_id",
                name = "Moodle user id",
                description = "Moodle auth id",
                isProtected = true
        )
        private val TelegramApiKey = Option.Str(
                id = "telegram_api_key",
                name = "Telegram Api Key",
                description = "The Telegram Api key for bot",
                isProtected = true
        )

        private val TelegramChatId = Option.Str(
                id = "telegram_chat_id",
                name = "Telegram Channel Id",
                description = "The Telegram Channel id for the bot"
        )
    }

    private fun String.title(): String {
        return "<i>${this}</i>"
    }

    private fun Course.getHebrewName(): String = try {
        Jsoup.parse(summary).getElementsByAttributeValueMatching("lang", "he")[0].text()
    } catch (e: Exception) {
        name
    }
}