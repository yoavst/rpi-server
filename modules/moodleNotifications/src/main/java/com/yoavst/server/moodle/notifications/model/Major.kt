package com.yoavst.server.moodle.notifications.model

data class Major(val name: String, val key: String)