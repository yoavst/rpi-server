package com.yoavst.server.moodle.notifications.model

import com.google.gson.annotations.SerializedName

data class ContentFile(@SerializedName("filename") val name: String, @SerializedName("fileurl") val url: String, @SerializedName("timemodified") val timeModified: String, val type: String) {
    companion object {
        const val TYPE_FILE = "file"
        const val TYPE_URL = "url"
    }
}