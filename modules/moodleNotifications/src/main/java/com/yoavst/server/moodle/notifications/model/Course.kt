package com.yoavst.server.moodle.notifications.model

import com.google.gson.annotations.SerializedName

data class Course(val id: Long, @SerializedName("shortname") val name: String, @SerializedName("enddate") val endDate: Long, val summary: String)