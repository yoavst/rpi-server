package com.yoavst.server.moodle.notifications.controller.mytau

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.FormElement

/**
 * Created by GevaK on 28/10/2016.
 * A very basic class for maintaining sessions with Jsoup - remembering cookies and headers.
 * Can definitely be improved, I only implemented the minimum functionality that I needed.
 */
class JsoupSession {
    var cookies: Map<String, String> = mutableMapOf()
    private var headers: List<Pair<String, String>> = mutableListOf()
    private val timeout: Int = 5000

    private fun setConnectionParams(conn: Connection): Connection = headers.fold(conn) { connection, (name, value) ->
        connection.header(name, value)
    }.cookies(cookies).timeout(timeout).validateTLSCertificates(false)

    fun submitForm(f: FormElement): Connection {
        val conn = f.submit()
        return setConnectionParams(conn)
    }

    fun connect(url: String): Connection {
        val conn = Jsoup.connect(url)
        return setConnectionParams(conn)
    }

    fun header(key: String, value: String): JsoupSession {
        this.headers += key to value
        return this
    }
}
