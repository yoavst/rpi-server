package com.yoavst.server.moodle.notifications.model

data class Content(val id: Int, val categoryName: String, val sectionName: String, val description: String, val files: List<ContentFile>, val modName: String, val instance: Int, val modIcon: String) {
}