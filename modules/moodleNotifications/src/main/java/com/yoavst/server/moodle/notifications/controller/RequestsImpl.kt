package com.yoavst.server.moodle.notifications.controller

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.interceptors.redirectResponseInterceptor
import com.github.kittinunf.fuel.core.interceptors.validatorResponseInterceptor
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.yoavst.server.Task
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.moodle.notifications.model.Content
import com.yoavst.server.moodle.notifications.model.ContentFile
import com.yoavst.server.moodle.notifications.model.Course
import org.jsoup.Jsoup
import java.io.File
import java.lang.reflect.Type
import java.net.HttpCookie
import java.net.URI
import java.util.*


class RequestsImpl(task: Task) : Requests, Logging(task, "Moodle notifications requests") {
    override fun getUserId(token: String): String {
        val (_, response, result) = "https://moodle.tau.ac.il/webservice/rest/server.php?wstoken=$token&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json"
                .httpGet()
                .responseString()

        require(response.statusCode == 200 && result is Result.Success<*, *>)
        logger.info { "Received site info" }

        return JsonParser().parse(result.get()).asJsonObject["userid"].asString
    }

    override fun getCourses(token: String, userMoodleId: String): List<Course> {
        val (_, response, result) = "https://moodle.tau.ac.il/webservice/rest/server.php?wstoken=$token&userid=$userMoodleId&wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json"
                .httpGet()
                .responseString()

        require(response.statusCode == 200 && result is Result.Success<*, *>)
        logger.info { "Received list of courses" }
        return gson.fromJson(result.get(), typeToken<List<Course>>())
    }

    override fun getContentsFor(course: Course, token: String): List<Content> {
        val (_, response, result) = "https://moodle.tau.ac.il/webservice/rest/server.php?wstoken=$token&wsfunction=core_course_get_contents&moodlewsrestformat=json&courseid=${course.id}"
                .httpGet()
                .responseString()
        require(response.statusCode == 200 && result is Result.Success<*, *>)
        logger.info { "Received contents for course: ${course.name}" }

        val categories = JsonParser().parse(result.get()).asJsonArray
        return categories.flatMap {
            val categoryJson = it.asJsonObject
            val categoryName = categoryJson["name"].asString
            categoryJson["modules"].asJsonArray.map {
                val contentJson = it.asJsonObject
                val id = contentJson["id"].asInt
                val contentName = contentJson["name"].asString
                val description = contentJson["description"]?.asString ?: ""
                val modName = contentJson["modname"]?.asString ?: ""
                val instance = contentJson["instance"]?.asInt ?: -1
                val modIcon = contentJson["modicon"]?.asString ?: ""
                val contents = contentJson["contents"]?.asJsonArray?.let { gson.fromJson<List<ContentFile>>(it, typeToken<List<ContentFile>>()) }
                        ?: emptyList()
                Content(id, categoryName, contentName, description, contents, modName, instance, modIcon)
            }
        }
    }

    override fun getAssignmentsFiles(course: Course, token: String): Map<Int, List<ContentFile>> {
        val (_, response, result) = "https://moodle.tau.ac.il/webservice/rest/server.php?wstoken=$token&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json&courseids[]=${course.id}"
                .httpGet()
                .responseString()
        require(response.statusCode == 200 && result is Result.Success<*, *>)
        logger.info { "Received assignments for course: ${course.name}" }

        val assignments = JsonParser().parse(result.get()).asJsonObject["courses"].asJsonArray[0].asJsonObject["assignments"].asJsonArray
        return assignments.associateBy({ it.asJsonObject["id"].asInt }) {
            it.asJsonObject["introattachments"]?.asJsonArray?.let { gson.fromJson<List<ContentFile>>(it, typeToken<List<ContentFile>>()) }
                    ?: emptyList()
        }
    }

    override fun downloadResource(resource: String, token: String, file: File) {
        val (_, response, result) = resource.appendParameter("token=$token").httpGet().response()
        require(response.statusCode == 200)
        logger.info { "Downloaded resource: $resource" }
        file.writeBytes(result.get())
    }

    override fun generateMoodleToken(username: String, id: String, password: String): String? = try {
        val initialMoodleSession = step1()
        val nextMoodleSession = step2(initialMoodleSession)
        val (samlSessionId, samlRequest, relayState, moodleSession) = step3(nextMoodleSession)
        var jsSessionId = step4(samlRequest, relayState)
        jsSessionId = step5(jsSessionId, username, id, password)
        val (samlResponse, relayState2) = step6(jsSessionId)
        val samlAuthToken = step7(samlResponse, relayState2, moodleSession, samlSessionId)
        val authSessionId = step8(moodleSession, samlSessionId, samlAuthToken)
        step9(authSessionId)
    } catch (e: Exception) {
        logger.error(e) { "Failed to login to moodle" }
        null
    }

    //region Login
    private fun step1(): String {
        val (_, response, _) = "https://moodle.tau.ac.il/".httpGet().response()

        require(response.statusCode == 200)
        logger.info { "Got initial moodle session" }

        return response.getCookie("MoodleSessionMoodle2018")!!
    }

    private fun step2(moodleSession: String): String = disableInterceptors {
        val (_, response, _) = "https://moodle.tau.ac.il/login/index.php"
                .httpGet()
                .header("Cookie" to "MoodleSessionMoodle2018=$moodleSession")
                .responseString()

        require(response.statusCode == 303)
        logger.info { "Redirected from login to auth successfully" }

        return response.getCookie("MoodleSessionMoodle2018") ?: moodleSession
    }

    data class Step3Result(val simpleSamlSessionId: String, val samlRequest: String, val relayState: String, val moodleSession: String)

    private fun step3(moodleSession: String): Step3Result {
        val (_, response, result) = "https://moodle.tau.ac.il/auth/saml2/login.php"
                .httpGet()
                .header("Cookie" to "MoodleSessionMoodle2018: $moodleSession")
                .responseString()

        require(response.statusCode == 200)
        logger.info { "Loaded auth successfully" }

        val newSession = response.getCookie("MoodleSessionMoodle2018") ?: moodleSession
        val samlSessionId = response.getCookie("SimpleSAMLSessionID")!!
        val body = Jsoup.parse(result.get()).body()
        val samlRequest = body.getElementsByAttributeValue("name", "SAMLRequest").first().attr("value")
        val relayState = body.getElementsByAttributeValue("name", "RelayState").first().attr("value")
        return Step3Result(samlSessionId, samlRequest, relayState, newSession)
    }

    private fun step4(samlRequest: String, relayState: String): String {
        val (_, response, _) = "https://nidp.tau.ac.il/nidp/saml2/sso"
                .httpPost(listOf("SAMLRequest" to samlRequest, "RelayState" to relayState))
                .header("Content-Type" to "application/x-www-form-urlencoded").response()

        require(response.statusCode == 200)
        logger.info { "Initiated SAML request successfully " }

        return response.getCookie("JSESSIONID")!!
    }

    private fun step5(jsSessionId: String, username: String, id: String, password: String): String {
        val (_, response, _) = "https://nidp.tau.ac.il/nidp/saml2/sso?sid=0&sid=0"
                .httpPost()
                .header("Content-Type" to "application/x-www-form-urlencoded", "Cookie" to "JSESSIONID=$jsSessionId")
                .body("option=credential&Ecom_User_ID=$username&Ecom_User_Pid=$id&Ecom_Password=$password")
                .response()

        require(response.statusCode == 200)
        logger.info { "Sent login info to server " }

        return response.getCookie("JSESSIONID")!!
    }

    data class Step6Result(val samlResponse: String, val relayState: String)

    private fun step6(jsSessionId: String): Step6Result {
        val (_, response, result) = "https://nidp.tau.ac.il/nidp/saml2/sso?sid=0"
                .httpGet()
                .header("Cookie" to "JSESSIONID=$jsSessionId")
                .responseString()

        require(response.statusCode == 200)
        logger.info { "Parsing SAML response from server" }

        val body = Jsoup.parse(result.get()).body()
        val samlResponse = body.getElementsByAttributeValue("name", "SAMLResponse").first().attr("value")
        val relayState = body.getElementsByAttributeValue("name", "RelayState").first().attr("value")
        return Step6Result(samlResponse, relayState)
    }

    private fun step7(samlResponse: String, relayState: String, moodleSession: String, simpleSamlSessionId: String): String = disableInterceptors {
        val (_, response, _) = "https://moodle.tau.ac.il/auth/saml2/sp/saml2-acs.php/moodle.tau.ac.il"
                .httpPost(listOf("SAMLResponse" to samlResponse, "RelayState" to relayState))
                .header("Content-Type" to "application/x-www-form-urlencoded", "Cookie" to "MoodleSessionMoodle2018=$moodleSession; SimpleSAMLSessionID=$simpleSamlSessionId")
                .response()

        require(response.statusCode == 303)
        logger.info { "Acquiring SAML auth token" }

        response.getCookie("SimpleSAMLAuthToken")!!
    }


    private fun step8(moodleSession: String, simpleSamlSessionId: String, simpleSamlAuthToken: String): String = disableInterceptors {
        val (_, response, _) = "https://moodle.tau.ac.il/auth/saml2/login.php"
                .httpGet()
                .header("Cookie" to "MoodleSessionMoodle2018=$moodleSession; SimpleSAMLSessionID=$simpleSamlSessionId; SimpleSAMLAuthToken=$simpleSamlAuthToken")
                .response()

        require(response.statusCode == 303)
        logger.info { "Acquiring final moodle session" }

        response.getCookie("MoodleSessionMoodle2018")!!
    }

    private fun step9(moodleSession: String): String = disableInterceptors {
        val (_, response, _) = "https://moodle.tau.ac.il/admin/tool/mobile/launch.php?service=moodle_mobile_app&passport=${Math.random() * 1000}&urlscheme=mobilemoodle"
                .httpGet()
                .header("Cookie" to "MoodleSessionMoodle2018=$moodleSession")
                .response()

        require(response.statusCode == 302)
        logger.info { "Acquiring moodle token" }

        String(Base64.getDecoder().decode(response.headers["Location"]!!.first().replaceFirst("mobilemoodle://token=", ""))).split(":::")[1]
    }

    private fun Response.getCookie(name: String): String? = headers["Set-Cookie"].orEmpty().flatMap(HttpCookie::parse).firstOrNull { it.name == name }?.value
    //endregion

    private val gson = Gson()
    private inline fun <reified T : Any> typeToken(): Type = object : TypeToken<T>() {}.type

    private fun String.appendParameter(appendQuery: String): String {
        val oldUri = URI(this)

        var newQuery = oldUri.query
        if (newQuery == null) {
            newQuery = appendQuery
        } else {
            newQuery += "&$appendQuery"
        }

        return URI(oldUri.scheme, oldUri.authority, oldUri.path, newQuery, oldUri.fragment).toString()
    }

    private inline fun <K> disableInterceptors(func: () -> K): K {
        FuelManager.instance.removeAllResponseInterceptors()
        val value = func()
        FuelManager.instance.addResponseInterceptor(validatorResponseInterceptor(200..299))
        FuelManager.instance.addResponseInterceptor(redirectResponseInterceptor(FuelManager.instance))
        return value
    }
}