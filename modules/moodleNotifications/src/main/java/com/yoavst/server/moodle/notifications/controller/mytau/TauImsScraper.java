package com.yoavst.server.moodle.notifications.controller.mytau;

import com.yoavst.server.moodle.notifications.model.Major;
import kotlin.Pair;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GevaK on 22/10/2016.
 */
public class TauImsScraper {
    // Browser Headers:
    final static private String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36";
    final static private String CONTENT_TYPE = "application/x-www-form-urlencoded";

    // URLs:
    final static private String URL_LOGIN = "https://www.ims.tau.ac.il/Tal/Login_Chk.aspx";
    final static private String URL_MAJORS = "https://www.ims.tau.ac.il/Tal/TP/Tziunim_P.aspx";
    final static private String URL_GRADES = "https://www.ims.tau.ac.il/Tal/TP/Tziunim_L.aspx";
    final static private String URL_SCANS = "https://www.ims.tau.ac.il/Tal/Scans/Scans_L.aspx";

    // Hebrew Text Strings
    //TODO: Read this from external file?
    final static private String NAME_TABLE_HEADER = "\u05e9\u05dd"; // Shem
    final static private String GRADE_TABLE_HEADER = "\u05e6\u05d9\u05d5\u05df"; // Ziun
    final static private String SCAN_TABLE_HEADER = "\u05d1\u05d7\u05e8"; // Bhar
    final static private String SCAN_AVAILABLE_STRING = "\u05d4\u05e6\u05d2\u05d4"; // Hazaga
    final static private String MAJOR_TABLE_HEADER = "\u05E9\u05DD \u05EA\u05DB\u05E0\u05D9\u05EA"; // Shem Tohnit

    String mId;
    boolean mLoggedIn;
    JsoupSession mSession;


    public TauImsScraper() {
        mSession = new JsoupSession();
        mSession = mSession.header("User-Agent", USER_AGENT);
        mSession = mSession.header("Content-Type", CONTENT_TYPE);
        // We set the timeout to 5 seconds, otherwise we get SocketTimeout on failed login because
        // TAU website waits 3 seconds before responding
        mLoggedIn = false;
    }

    /**
     * Logs in to TAU IMS system with given credentials,
     * saves the cookies for later use.
     */
    public void login(String idNumber, String username, String password) throws InternetConnectionException, LoginFailedException {

        try {
            Connection.Response resp = mSession.connect(URL_LOGIN)
                    .data("txtUser", username)
                    .data("txtId", idNumber)
                    .data("txtPass", password)
                    .data("enter", "")
                    .data("javas", "1")
                    .method(Connection.Method.POST)
                    .execute();
            Document doc = resp.parse();

            mId = idNumber;
            mSession.setCookies(resp.cookies());

            if (resp.cookies().containsKey("ASP.NET_SessionId") &&
                    resp.cookies().containsKey("TauTalId") &&
                    resp.cookies().containsKey("TauTalUser")) {

                mLoggedIn = true;
            } else {
                throw new LoginFailedException("TAU IMS login failed - didn't receive cookies");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternetConnectionException("Error posting login: " + e.getClass() + " - " + e.getMessage());
        }
    }

    public ArrayList<Major> getMajors() throws InternetConnectionException, WebParsingException {
        ArrayList<Major> result = new ArrayList<>();
        Document doc;
        try {
            doc = mSession.connect(URL_MAJORS +
                    "?id=" + this.mId)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternetConnectionException("Error posting default grades form: " + e.getClass() + " - " + e.getMessage());
        }
        Element majorsTable = doc.select("table[class=tableblds]").first();
        if (majorsTable == null) {
            // Raise exception, no majors found
            throw new WebParsingException("Failed to find any majors, no table found");
        }
        ArrayList<String> majorNames = table2arr(majorsTable, MAJOR_TABLE_HEADER);
        Elements tckeys = doc.select("input[name=tckey]");
        if (majorNames.size() != tckeys.size()) {
            throw new WebParsingException("Failed to parse majors, number of tckeys does not match number of columns");
        }
        for (int i = 0; i < majorNames.size(); i++) {
            result.add(new Major(majorNames.get(i), tckeys.get(i).val()));
        }
        return result;
    }

    /**
     * Returns grades for all courses in given semester and major.
     * The semester is given as an object, and the major just as a string of the tckey.
     * If no grades are found this throws a WebParsingException as the user probably did not intend to pick a semester in which he took no courses.
     */
    public HashMap<String, String> getCourseGrades(int academicYear, int academicSemester, Major major) throws InternetConnectionException, WebParsingException {
        // Obtain grades for last semester
        Document last_sem_doc;
        try {
            last_sem_doc = mSession.connect(URL_GRADES +
                    "?id=" + this.mId)
                    .data("tckey", major.getKey())
                    .data("peula", "3")
                    .data("javas", "1")
                    .data("caller", "tziunim_p")
                    .post();
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternetConnectionException("Error posting default grades form: " + e.getClass() + " - " + e.getMessage());
        }
        // Obtain grades for requested semester
        FormElement form = (FormElement) last_sem_doc.select("form#frmgrid").first();
        // Modify lstSem value in form data to requested semester
        // We use an ugly hack to do this because Jsoup does not support any easy way to modify
        // form data after parsing it
        Elements lstSemSelect = form.select("select[name=lstSem]");
        lstSemSelect.html("");

        // Post form with modified semester
        Connection.Response resp;
        Document sem_doc;
        try {
            resp = mSession.submitForm(form)
                    .data("lstSem", "" + academicYear + "" + academicSemester)
                    .execute();
            sem_doc = resp.parse();
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternetConnectionException("Error posting grades form: " + e.getClass() + " - " + e.getMessage());
        }

        //TODO: Long course names are truncated because TAU website is bad. Solve this?

        // Select table
        Element gradesTable = sem_doc.select("table[class=table]").first();
        if (gradesTable == null) {
            // We found no tables at all, throw exception since the user probably did not intend to select this semester
            throw new WebParsingException("Did not find any grades for semester " + academicYear + "" + academicSemester + ". Grades table not found in document.");
            //return new HashMap<>();
        }
        HashMap<String, String> result;
        try {
            result = table2map(gradesTable, NAME_TABLE_HEADER, GRADE_TABLE_HEADER);
        } catch (WebParsingException e) {
            e.printStackTrace();
            throw new WebParsingException("Did not find any grades for semester " + academicYear + "" + academicSemester + ". Errmsg: " + e.getMessage());
            //result = new HashMap<>(); // Found no grades, possibly empty table without headers
        }
        return result;
    }

    /**
     * Returns scans exist (true / false) for all courses in given semester and major.
     * The semester is given as an object, and the major just as a string of the tckey.
     * If no grades are found this DOES NOT throw an exception as it is possible to have no scans in a given semester.
     */
    public HashMap<String, Boolean> getExamScans(int academicYear, int academicSemester, Major major) throws InternetConnectionException {
        Document doc;
        try {
            doc = mSession.connect(URL_SCANS +
                    "?id=" + this.mId)
                    .data("tckey", major.getKey())
                    .data("peula", "3")
                    .data("javas", "1")
                    .data("caller", "scans_p")
                    .data("sem", "" + academicYear + "" + academicSemester)
                    .data("currsem","" + academicYear + "" + academicSemester)
                    .post();
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternetConnectionException("Error posting scans form: " + e.getClass() + " - " + e.getMessage());
        }

        // Select table
        Element scansTable = doc.select("table[class=tablebld]").first();
        if (scansTable == null) {
            // Return empty map, we found no scans
            return new HashMap<>();
        }
        HashMap<String, String> scanStrings;
        try {
            scanStrings = table2map(scansTable, NAME_TABLE_HEADER, SCAN_TABLE_HEADER);
        } catch (WebParsingException e) {
            e.printStackTrace();
            scanStrings = new HashMap<>(); // Found no scans, possibly empty table without headers
        }

        // Map the strings to booleans which indicate if a scan is available
        HashMap<String, Boolean> result = new HashMap<>(scanStrings.size());
        for (String course : scanStrings.keySet()) {
            result.put(course, scanStrings.get(course).equals(SCAN_AVAILABLE_STRING));
        }
        return result;
    }

    /**
     * Notice that this is different than table2map as it regards the headers as the first <tr>
     * rather than <th>, because that is how TAU website is built :(
     */
    public static ArrayList<String> table2arr(Element table, String header) throws WebParsingException {

        ArrayList<String> result = new ArrayList<>();
        Elements ths = table.select("tr").first().select("td,th");
        int colIndex = -1;
        // Find correct column index for given header
        for (int i = 0; i < ths.size(); i++) {
            Element th = ths.get(i);
            if (th.text().contains(header)) {
                colIndex = i;
            }
        }
        if (colIndex == -1) {
            throw new WebParsingException("Failed to find expected header " + header + " in the header row: " + ths);
        }
        Element tr = ths.get(0).parent().nextElementSibling(); // Find only the rows after the header
        while (tr != null) {
            Elements tds = tr.select("td");
            if (tds.size() != 1) {
                // Ignore metadata rows (like "Lo leshiklul")
                String row_val = tds.get(colIndex).text().trim();
                result.add(row_val);
            }
            tr = tr.nextElementSibling();
        }

        return result;

    }

    public static HashMap<String, String> table2map(Element table, String keyHeader, String valueHeader) throws WebParsingException {
        HashMap<String, String> result = new HashMap<String, String>();
        Elements ths = table.select("th");
        int keyIndex = -1;
        int valueIndex = -1;
        // Find correct column indices for given headers
        for (int i = 0; i < ths.size(); i++) {
            Element th = ths.get(i);
            if (th.text().contains(keyHeader)) {
                keyIndex = i;
            } else if (th.text().contains(valueHeader)) {
                valueIndex = i;
            }
        }
        if (keyIndex == -1) {
            throw new WebParsingException("Failed to find expected header " + keyHeader + " in the header row: " + ths);
            //TODO: Make sure that we handle gracefully the case where the user has no grades (chose bad semester by mistake)
        }
        if (valueIndex == -1) {
            throw new WebParsingException("Failed to find expected header " + valueHeader + " in the header row: " + ths);
        }
        Element tr = ths.get(0).parent().nextElementSibling(); // Find only the rows after the header
        while (tr != null) {
            Elements tds = tr.select("td");
            if (tds.size() != 1) {
                // Ignore metadata rows (like "Lo leshiklul")
                String key = tds.get(keyIndex).text().trim();
                String value = tds.get(valueIndex).text().trim();
                result.put(key, value);
            }
            tr = tr.nextElementSibling();
        }

        return result;

    }

    static class ScraperException extends Exception {
        ScraperException(String error) {
            super(error);
        }
    }

    static class WebParsingException extends ScraperException {
        WebParsingException(String error) {
            super(error);
        }
    }

    static class InternetConnectionException extends ScraperException {
        InternetConnectionException(String error) {
            super(error);
        }
    }

    static class LoginFailedException extends ScraperException {
        LoginFailedException(String error) {
            super(error);
        }
    }
}

