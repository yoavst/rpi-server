package com.yoavst.server.moodle.notifications.controller

import com.yoavst.server.moodle.notifications.model.Content
import com.yoavst.server.moodle.notifications.model.Course
import com.yoavst.server.moodle.notifications.model.Major

interface Storage {
    var courses: List<Course>

    var majors: List<Major>
    var grades: Map<String, String>
    var scans: Map<String, Boolean>

    fun contentsFor(course: Course): List<Content>
    fun setContentsFor(course: Course, content: List<Content>)
}