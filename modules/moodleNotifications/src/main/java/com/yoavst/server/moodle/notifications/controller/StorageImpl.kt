package com.yoavst.server.moodle.notifications.controller

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.moodle.notifications.model.Content
import com.yoavst.server.moodle.notifications.model.Course
import com.yoavst.server.moodle.notifications.model.Major
import java.lang.reflect.Type

class StorageImpl(private val task: Task) : Storage {
    override var courses: List<Course>
        get() {
            if (!coursesListFile.exists() || coursesListFile.length() == 0L) return emptyList()
            return coursesListFile.reader().use { gson.fromJson(JsonReader(it), typeToken<List<Course>>()) }
        }
        set(value) = coursesListFile.apply { createNewFile() }.writer().use { gson.toJson(value, it) }

    override var majors: List<Major>
        get() {
            if (!majorsListFile.exists() || majorsListFile.length() == 0L) return emptyList()
            return majorsListFile.reader().use { gson.fromJson(JsonReader(it), typeToken<List<Major>>()) }
        }
        set(value) = majorsListFile.apply { createNewFile() }.writer().use { gson.toJson(value, it) }

    override var grades: Map<String, String>
        get() {
            if (!gradesFile.exists() || gradesFile.length() == 0L) return emptyMap()
            return gradesFile.reader().use { gson.fromJson(JsonReader(it), typeToken<Map<String, String>>()) }
        }
        set(value) = gradesFile.apply { createNewFile() }.writer().use { gson.toJson(value, it) }

    override var scans: Map<String, Boolean>
        get() {
            if (!scansFile.exists() || scansFile.length() == 0L) return emptyMap()
            return scansFile.reader().use { gson.fromJson(JsonReader(it), typeToken<Map<String, Boolean>>()) }
        }
        set(value) = scansFile.apply { createNewFile() }.writer().use { gson.toJson(value, it) }

    override fun contentsFor(course: Course): List<Content> {
        val file = fileForCourse(course)
        if (!file.exists() || file.length() == 0L) return emptyList()
        return file.reader().use { gson.fromJson(JsonReader(it), typeToken<List<Content>>()) }
    }
    override fun setContentsFor(course: Course, content: List<Content>) =
            fileForCourse(course).apply { createNewFile() }.writer().use { gson.toJson(content, it) }

    private val coursesListFile by lazy { Environment.storage.file(task, "courses.json") }
    private fun fileForCourse(course: Course) = Environment.storage.file(task, "course_${course.id}.json")

    private val majorsListFile by lazy { Environment.storage.file(task, "majors.json") }
    private val gradesFile by lazy { Environment.storage.file(task, "grades.json") }
    private val scansFile by lazy { Environment.storage.file(task, "scans.json") }

    private val gson = Gson()
    private inline fun <reified T : Any> typeToken(): Type = object : TypeToken<T>() {}.type
}