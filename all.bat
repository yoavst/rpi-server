@ECHO OFF
CALL gradle :modules:amanNotifications:shadowJar :modules:ohelshemNotifications:shadowJar :modules:ohelshemBackup:shadowJar :modules:moodleNotification:shadowJar :modules:sample:shadowJar

CALL xcopy "modules\amanNotifications\build\libs\*.jar" "output\modules\"
CALL xcopy "modules\ohelshemNotifications\build\libs\*.jar" "output\modules\"
CALL xcopy "modules\ohelshemBackup\build\libs\*.jar" "output\modules\"
CALL xcopy "modules\moodleNotifications\build\libs\*.jar" "output\modules\"
CALL xcopy "modules\sample\build\libs\*.jar" "output\modules\"


CALL gradle :runner:shadowJar

CALL xcopy "runner\build\libs\*.jar" "output\"
CALL rename "output\runner-all.jar" "yoav_server_v1.jar"

CALL xcopy "original.jpg" "output\"
CALL xcopy "empty_history.db" "output\"
CALL xcopy "run.sh" "output\"
CALL xcopy "run.bat" "output\"

CALL echo 122345678 > output/tokens.txt

