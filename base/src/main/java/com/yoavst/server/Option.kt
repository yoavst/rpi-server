package com.yoavst.server

import org.threeten.bp.LocalDate

sealed class Option<out K : Any>(val id: String, val name: String, val description: String, val defaultValue: K, val options: List<K>?) {

    class Integer(id: String, name: String, description: String, defaultValue: Int = 0, options: List<Int>? = null):
            Option<Int>(id, name, description, defaultValue, options)

    class Decimal(id: String, name: String, description: String, defaultValue: Double, options: List<Double>? = null):
            Option<Double>(id, name, description, defaultValue, options)

    class Bool(id: String, name: String, description: String, defaultValue: Boolean = false, options: List<Boolean>? = null):
            Option<Boolean>(id, name, description, defaultValue, options)

    class Str(id: String, name: String, description: String, defaultValue: String = "", val isProtected: Boolean = false, options: List<String>? = null):
            Option<String>(id, name, description, defaultValue, options)

    class Date(id: String, name: String, description: String, defaultValue: LocalDate, options: List<LocalDate>? = null):
            Option<LocalDate>(id, name, description, defaultValue, options)

    class Duration(id: String, name: String, description: String, defaultValue: Duration, options: List<Duration>? = null):
            Option<Duration>(id, name, description, defaultValue, options)
}