package com.yoavst.server

import com.yoavst.server.logging.RawLogger
import kotlin.properties.Delegates.notNull

object Environment {
    var storage: Storage by notNull()

    var loggerFactory: (task: Task, name: String, clazz: Class<*>) -> RawLogger by notNull()

    fun rawLoggerOf(task: Task, name: String, clazz: Class<*>): RawLogger = loggerFactory(task, name, clazz)
}