package com.yoavst.server

interface Invokable: () -> TaskResults {
    val id: String

    companion object {
        operator fun invoke(id: String, func: () -> TaskResults) = object : Invokable {
            override val id: String = id

            override fun invoke(): TaskResults = func()
        }
    }
}