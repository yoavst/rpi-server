package com.yoavst.server.logging.impl

import com.yoavst.server.Task
import com.yoavst.server.logging.Loggable
import com.yoavst.server.logging.impl.internal.LoggerFactory

open class Logging(final override val task: Task, final override val name: String): Loggable {
    override val logger: TaskLogger = LoggerFactory.logger(task, javaClass, name)
}