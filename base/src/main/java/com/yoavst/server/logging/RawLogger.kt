package com.yoavst.server.logging

interface RawLogger {
    val level: Int

    fun log(level: Int, message: Any?)
    fun log(level: Int, throwable: Throwable, message: Any?)

    companion object {
        const val Debug = 1
        const val Info = 2
        const val Warn = 3
        const val Error = 4
        const val Off = 5
    }
}