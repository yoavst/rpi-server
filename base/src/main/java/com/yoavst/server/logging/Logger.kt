package com.yoavst.server.logging

import com.yoavst.server.logging.RawLogger.Companion.Debug
import com.yoavst.server.logging.RawLogger.Companion.Error
import com.yoavst.server.logging.RawLogger.Companion.Info
import com.yoavst.server.logging.RawLogger.Companion.Warn

open class Logger(rawLogger: RawLogger) : RawLogger by rawLogger {
    val isDebugEnabled: Boolean
        get() = Debug >= level

    val isInfoEnabled: Boolean
        get() = Info >= level

    val isWarnEnabled: Boolean
        get() = Warn >= level

    val isErrorEnabled: Boolean
        get() = Error >= level

    fun debug(message: Any?) = log(Debug, message)
    fun info(message: Any?) = log(Info, message)
    fun warn(message: Any?) = log(Warn, message)
    fun error(message: Any?) = log(Error, message)

    fun debug(throwable: Throwable, message: Any?) = log(Debug, throwable, message)
    fun info(throwable: Throwable, message: Any?) = log(Info, throwable, message)
    fun warn(throwable: Throwable, message: Any?) = log(Warn, throwable, message)
    fun error(throwable: Throwable, message: Any?) = log(Error, throwable, message)
}