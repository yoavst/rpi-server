package com.yoavst.server.logging.impl.internal

import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.logging.impl.TaskLogger

object LoggerFactory {
    fun logger(task: Task, clazz: Class<*>, name: String): TaskLogger {
        return TaskLogger(task, Environment.rawLoggerOf(task, name, clazz))
    }
}