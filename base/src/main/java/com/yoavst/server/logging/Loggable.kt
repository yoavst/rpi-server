package com.yoavst.server.logging

import com.yoavst.server.Task
import com.yoavst.server.logging.impl.TaskLogger
import com.yoavst.server.logging.impl.internal.LoggerFactory

interface Loggable {
    val name: String
    val task: Task

    val logger: TaskLogger

    fun logger(): TaskLogger = logger(name)
    fun logger(name: String): TaskLogger = LoggerFactory.logger(task, javaClass, name)
}