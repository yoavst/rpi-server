package com.yoavst.server.logging.impl

import com.yoavst.server.Task
import com.yoavst.server.logging.Logger
import com.yoavst.server.logging.RawLogger


open class TaskLogger(val task: Task, rawLogger: RawLogger) : Logger(rawLogger) {
    /**
     * Lazy add a log message if isDebugEnabled is true
     */
    inline fun debug(msg: () -> Any?) {
        if (isDebugEnabled) debug(msg())
    }

    /**
     * Lazy add a log message if isInfoEnabled is true
     */
    inline fun info(msg: () -> Any?) {
        if (isInfoEnabled) info(msg())
    }

    /**
     * Lazy add a log message if isWarnEnabled is true
     */
    inline fun warn(msg: () -> Any?) {
        if (isWarnEnabled) warn(msg())
    }

    /**
     * Lazy add a log message if isErrorEnabled is true
     */
    inline fun error(msg: () -> Any?) {
        if (isErrorEnabled) error(msg())
    }

    /**
     * Lazy add a log message with throwable payload if isDebugEnabled is true
     */
    inline fun debug(t: Throwable, msg: () -> Any?) {
        if (isDebugEnabled) debug(t, msg())
    }

    /**
     * Lazy add a log message with throwable payload if isInfoEnabled is true
     */
    inline fun info(t: Throwable, msg: () -> Any?) {
        if (isInfoEnabled) info(t, msg())
    }

    /**
     * Lazy add a log message with throwable payload if isWarnEnabled is true
     */
    inline fun warn(t: Throwable, msg: () -> Any?) {
        if (isWarnEnabled) warn(t, msg())
    }

    /**
     * Lazy add a log message with throwable payload if isErrorEnabled is true
     */
    inline fun error(t: Throwable, msg: () -> Any?) {
        if (isErrorEnabled) error(t, msg())
    }
}