package com.yoavst.server

import com.yoavst.server.logging.Loggable
import com.yoavst.server.logging.impl.TaskLogger
import com.yoavst.server.logging.impl.internal.LoggerFactory

/**
 * A base task class for the task runner.
 */
abstract class Task(
        /**
         * The id of the task.
         */
        override val id: String,
        /**
         * Readable name of the task.
         */
        override val name: String,
        /**
         * Description of the task.
         */
        val description: String) : Loggable, Invokable {

    override val task: Task
        get() = this

    override val logger: TaskLogger by lazy { LoggerFactory.logger(this, javaClass, javaClass.simpleName) }

    open val settings: List<Option<*>> = emptyList()
    open val subTasks: List<SubTask> = emptyList()

    protected fun <K : Any> query(option: Option<K>): K = Environment.storage[task, option]

    protected fun <K : Any> set(option: Option<K>, value: K?) {
        Environment.storage[task, option] = value
    }

    inline protected fun <K : Any> set(option: Option<K>, value: () -> K?) {
        Environment.storage[task, option] = value()
    }

    override fun equals(other: Any?): Boolean = other is Task && id == other.id

    override fun hashCode(): Int = id.hashCode()
}