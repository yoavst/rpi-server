package com.yoavst.server

import com.yoavst.server.logging.impl.TaskLogger
import com.yoavst.server.logging.impl.internal.LoggerFactory


abstract class SubTask(final override val task: Task, id: String, name: String, description: String): Task(id, name, description) {
    override val logger: TaskLogger by lazy { LoggerFactory.logger(this, javaClass, name) }

    final override val settings: List<Option<*>>
        get() = task.settings

    override val subTasks: List<SubTask>
        get() = task.subTasks

    override fun equals(other: Any?): Boolean = other is SubTask && id == other.id && task.id == other.task.id
    override fun hashCode(): Int = id.hashCode()
}