package com.yoavst.server

import java.io.File

interface Storage {
    fun file(task: Task, name: String, create: Boolean = true): File
    fun file(name: String): File
    fun folder(task: Task): File
    fun mainFile(name: String, create: Boolean = true): File

    val modules: List<File>

    operator fun <K : Any> get(task: Task, option: Option<K>): K
    operator fun <K : Any> set(task: Task, option: Option<K>, value: K?)

}

