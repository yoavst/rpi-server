package com.yoavst.server

import com.yoavst.server.TaskResults.Scheduling.Normal
import org.threeten.bp.Duration

data class TaskResults(val completed: Boolean, val scheduling: Scheduling = Normal) {
    sealed class Scheduling {
        object Normal : Scheduling()
        class Retry(val duration: Duration): Scheduling()
    }

    companion object {
        fun success() = TaskResults(completed = true)
        fun failure() = TaskResults(completed = false)
        fun retry(duration: Duration) = TaskResults(completed = false, scheduling = Scheduling.Retry(duration))
    }
}