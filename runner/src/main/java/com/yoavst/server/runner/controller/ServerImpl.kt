package com.yoavst.server.runner.controller

import com.yoavst.server.Storage
import com.yoavst.server.SubTask
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.TaskResults.Scheduling.Retry
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.runner.PrefsName
import com.yoavst.server.runner.TaskClassNameInManifest
import com.yoavst.server.runner.TaskDefaultSchedulingRuleInManifest
import com.yoavst.server.runner.controller.scheduling.SchedulerService
import com.yoavst.server.runner.controller.storage.Preferences
import com.yoavst.server.runner.controller.storage.JsonPreferences
import com.yoavst.server.runner.logging.storage.HistoryItem
import com.yoavst.server.runner.logging.storage.HistoryStorage
import com.yoavst.server.runner.model.ManagedTask
import com.yoavst.server.runner.model.SchedulableTask
import com.yoavst.server.runner.util.peek
import java.io.File
import java.net.URLClassLoader
import java.util.*
import java.util.jar.JarFile

class ServerImpl(task: Task, private val storage: Storage, private val scheduler: SchedulerService) :
        Logging(task, "Server"), Server {
    private val prefs: Preferences = JsonPreferences(storage.mainFile(PrefsName))

    init {
        scheduler.taskInterceptor = { task -> run(task) }
    }

    override lateinit var tasks: MutableList<ManagedTask>

    fun add(task: Task, rule: String) {
        logger.info { "modules has loaded: ${task.name}. Total modules: ${tasks.size}" }
        if (task !is ManagedTask)
            tasks.add(task.toManaged(rule))
        else
            tasks.add(task)
    }

    override fun loadModule(file: File): Boolean {
        require(file.extension == "jar" && file.exists())

        val newTasks = loadModules(listOf(file))
        if (newTasks.isEmpty()) {
            logger.info { "Failed to load module: ${file.name}" }
            return false
        } else {
            logger.info { "Successfully load module: ${file.name}" }
            add(newTasks[0].task, newTasks[0].rule)
            return true
        }
    }

    override fun start() {
        HistoryStorage.open()
        Runtime.getRuntime().addShutdownHook(Thread(Runnable {
            HistoryStorage.close()
        }))
        logger.info { "Starting server" }
        val availableModules = storage.modules
        logger.info { "Loading Modules: ${availableModules.size} available" }
        tasks = loadModules(availableModules).peek {
            if (scheduleRuleFor(it.task) == null)
                setScheduleRuleFor(it.task, it.rule)
        }.map {
            it.task.toManaged(it.rule)
        }.toMutableList()
        logger.info { "${tasks.size} modules has loaded:" }
        tasks.forEachIndexed { i, task ->
            logger.info { "${i + 1}. ${task.name}" }
        }

        scheduleTasks()
        scheduler.start()
    }

    override fun run(task: Task): TaskResults {
        logger.info { "Start Running task: ${task.name}" }
        val before = System.currentTimeMillis()
        try {
            val result = task()
            val endTime = System.currentTimeMillis()
            logger.info { "${result.pretty()} [${endTime - before} ms] Finished Running task: ${task.name}" }

            val scheduling = (result.scheduling as? TaskResults.Scheduling.Retry)?.duration?.toMillis() ?: HistoryItem.NoScheduling
            HistoryStorage.log(HistoryItem(task.id, before, endTime, result.completed, scheduling))
            return result
        } catch (exception: Exception) {
            val time = System.currentTimeMillis()
            logger.error(exception) { "Failed to run task: ${task.name}" }
            HistoryStorage.log(HistoryItem(task.id, before, time))
            return TaskResults.failure()
        }
    }

    override fun run(task: SubTask): TaskResults {
        logger.info { "Start Running subtask: ${task.name} for task ${task.task.name}" }
        val before = System.currentTimeMillis()
        try {
            val result = task()
            val endTime = System.currentTimeMillis()
            logger.info { "${result.pretty()} [${endTime - before} ms] Finished Running subtask: ${task.name}" }

            val scheduling = (result.scheduling as? TaskResults.Scheduling.Retry)?.duration?.toMillis() ?: HistoryItem.NoScheduling
            HistoryStorage.log(HistoryItem(task.id, before, endTime, result.completed, scheduling))
            return result
        } catch (exception: Exception) {
            val time = System.currentTimeMillis()
            logger.error(exception) { "Failed to run subtask: $task" }
            HistoryStorage.log(HistoryItem(task.id, before, time))
            return TaskResults.failure()
        }
    }

    override fun sync() {
        logger.info { "Performing Sync to tasks." }
        scheduleTasks()
    }

    private fun scheduleTasks() {
        for (task in tasks) {
            if (task.enabled)
                scheduler.schedule(scheduleRuleFor(task)!!, task)
            else {
                scheduler.stopSchedule(task)
            }
        }
    }

    private fun TaskResults.pretty(): String {
        return buildString {
            append('[')
            if (completed)
                append("Success")
            else
                append("Failure")
            val scheduling = scheduling
            if (scheduling is Retry) {
                append(' ')
                append(scheduling.duration)
            }
            append(']')
        }
    }

    private fun onTaskModeChanged(task: ManagedTask) {
        setEnabledFor(task, task.enabled)
        setScheduleRuleFor(task, task.rule)
    }

    private fun Task.toManaged(rule: String) = ManagedTask(this, enabledFor(this) ?: true, rule) {
        onTaskModeChanged(it)
    }

    private fun enabledFor(task: Task) = prefs.getBool(task.id + TaskEnabled)
    private fun setEnabledFor(task: Task, enabled: Boolean) {
        prefs[task.id + TaskEnabled] = enabled
    }

    private fun scheduleRuleFor(task: Task) = prefs.getString(task.id + TaskSchedulingRule)
    private fun setScheduleRuleFor(task: Task, rule: String) {
        prefs[task.id + TaskSchedulingRule] = rule
    }

    //region Util
    private fun loadModules(modules: List<File>): List<SchedulableTask> {
        val loader = URLClassLoader(modules.map { it.toURI().toURL() }.toTypedArray(), javaClass.classLoader)
        val tasks: MutableList<SchedulableTask> = ArrayList(modules.size)
        for ((index, module) in modules.withIndex()) {
            try {
                logger.debug { "Trying to load module ${index + 1}: ${module.name}" }
                val jar = JarFile(module)
                val manifest = jar.manifest ?: throw IllegalArgumentException("No Manifest available.")
                val className = manifest.mainAttributes.getValue(TaskClassNameInManifest)?.toString()
                        ?: throw IllegalArgumentException("No Task class specified.")
                val defaultScheduling = manifest.mainAttributes.getValue(TaskDefaultSchedulingRuleInManifest)?.toString()
                        ?: throw IllegalArgumentException("No default scheduling rule specified.")
                logger.debug { "Module ${index + 1} has task class named: $className" }
                val clazz = Class.forName(className, true, loader).asSubclass(Task::class.java)
                val constructor = clazz.getConstructor()
                val task: Task = constructor.newInstance()

                tasks += SchedulableTask(task, defaultScheduling)

                logger.debug { "Module ${index + 1} successfully loaded: ${task.name}" }
            } catch (exception: IllegalArgumentException) {
                logger.error(exception) { "Encountered error while trying to load module ${module.name} " }
            }
        }
        return tasks
    }
    //endregion


    companion object {
        val TaskEnabled = "_enabled"
        val TaskSchedulingRule = "_rule"
    }
}