package com.yoavst.server.runner.controller.storage

import com.google.gson.Gson
import org.threeten.bp.Duration
import org.threeten.bp.LocalDate
import java.io.File
import java.util.*
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import org.aaronhe.threetengson.ThreeTenGsonAdapter

class JsonPreferences(private val file: File) : Preferences {
    private val cache: MutableMap<String, Any>

    init {
        cache = if (!file.exists()) HashMap<String, Any>(5) else {
            val text = file.readText()
            if (text.isNotEmpty()) gson.fromJson(file.readText())
            else HashMap<String, Any>(5)
        }
    }

    override fun getInteger(id: String): Int? {
        return cache[id]?.let {
            it as? Int ?: (it as? Double)?.toInt() ?: (it as? String)?.toInt()
        }
    }

    override fun getDecimal(id: String): Double? {
        return cache[id]?.let {
            it as? Double ?: (it as? String)?.toDouble()
        }
    }

    override fun getBool(id: String): Boolean? {
        return cache[id] as? Boolean
    }

    override fun getString(id: String): String? {
        return cache[id] as? String
    }

    override fun getDate(id: String): LocalDate? {
        return cache[id] as? LocalDate
    }

    override fun getDuration(id: String): Duration? {
        val data = cache[id] as? Long ?: return null
        return Duration.ofMillis(data)
    }

    override fun set(id: String, value: Any?) {
        if (value == null) {
            cache.remove(id)
        } else {
            if (value is Duration)
                cache[id] = value.toMillis()
            else
                cache[id] = value
        }
        if (!file.exists())
            file.parentFile.mkdirs()
        file.writeText(gson.toJson(cache))
    }

    companion object {
        private val gson: Gson = ThreeTenGsonAdapter.registerAll(GsonBuilder()).create()
    }
}