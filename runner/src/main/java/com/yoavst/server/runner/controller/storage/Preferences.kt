package com.yoavst.server.runner.controller.storage

import org.threeten.bp.Duration
import org.threeten.bp.LocalDate

interface Preferences {
    fun getInteger(id: String): Int?
    fun getDecimal(id: String): Double?
    fun getBool(id: String): Boolean?
    fun getString(id: String): String?
    fun getDate(id: String): LocalDate?
    fun getDuration(id: String): Duration?

    operator fun set(id: String, value: Any?)

}