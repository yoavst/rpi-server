package com.yoavst.server.runner.logging.storage

import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.runner.DatabaseName
import com.yoavst.server.runner.util.BaseTable
import com.yoavst.server.runner.util.Field
import com.yoavst.server.runner.util.sqlEscapeString
import org.tmatesoft.sqljet.core.SqlJetTransactionMode
import org.tmatesoft.sqljet.core.table.ISqlJetCursor
import org.tmatesoft.sqljet.core.table.SqlJetDb
import java.io.File
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object HistoryStorage {
    private lateinit var db: SqlJetDb
    private lateinit var executor: ExecutorService

    fun open() {
        executor = Executors.newSingleThreadExecutor()
        execute {
            val file = Environment.storage.mainFile(DatabaseName, create = false)
            if (file.length() == 0L) {
                Environment.storage.file("empty_history.db").inputStream().copyTo(file.outputStream())
            }
            db = SqlJetDb.open(file, true)
        }
    }

    fun close() {
        execute {
            db.close()
        }
    }


    fun log(task: Task, log: String) {
        execute {
            commit {
                val table = table(LogsTable.TableName)
                table.insert(task.id, sqlEscapeString(log))
            }
        }
    }

    fun log(historyItem: HistoryItem) {
        execute {
            commit {
                val table = table(HistoryTable.TableName)
                table.insert(historyItem.taskId, historyItem.startTime, historyItem.endTime,
                        if (historyItem.isCompleted) 1 else 0, historyItem.scheduling)
            }
        }
    }

    fun getHistory(count: Int, task: Task): List<HistoryItem> = getHistory(count, task.id)

    fun getHistory(count: Int, task: String? = null): List<HistoryItem> {
        return submit {
            readCommit {
                val table = table(HistoryTable.TableName)
                val cursor = if (task == null) table.open() else table.lookup(HistoryTable.TaskIndex, task)
                cursor.reverse().use {
                    val list = LinkedList<HistoryItem>()
                    forEach(count) {
                        list.add(HistoryItem(
                                getString(HistoryTable.Task.column),
                                getInteger(HistoryTable.Start.column),
                                getInteger(HistoryTable.End.column),
                                getBoolean(HistoryTable.Completed.column),
                                getInteger(HistoryTable.Scheduling.column)
                        ))
                    }
                    list
                }

            }
        }
    }

    fun getLogs(count: Int, task: Task): List<String> = getLogs(count, task.id)

    fun getLogs(count: Int, task: String? = null): List<String> {

        return submit {
            readCommit {
                val table = table(LogsTable.TableName)
                val cursor = if (task == null) table.open() else table.lookup(LogsTable.TaskIndex, task)
                cursor.reverse().use {
                    val list = LinkedList<String>()
                    forEach(count) {
                        list.add(getString(LogsTable.Message.column))
                    }
                    list
                }

            }
        }
    }

    fun clearAllLogs() {
        execute {
            commit {
                val deleteCursor = table(LogsTable.TableName).open()
                deleteCursor.forEach {
                    deleteCursor.delete()
                }
            }
        }
    }

    fun clearLogsFor(task: Task) = clearLogsFor(task.id)

    fun clearLogsFor(task: String) {
        execute {
            commit {
                val deleteCursor = table(LogsTable.TableName).lookup(LogsTable.TaskIndex, task)
                deleteCursor.forEach {
                    deleteCursor.delete()
                }
            }
        }
    }

    private fun execute(func: () -> Unit) {
        executor.execute(func)
    }

    private inline fun <T> submit(crossinline func: () -> T): T {
        return executor.submit(Callable { func() }).get()
    }

    private inline fun commit(func: () -> Unit) {
        db.beginTransaction(SqlJetTransactionMode.WRITE)
        try {
            func()
        } finally {
            db.commit()
        }
    }

    private inline fun <T> readCommit(func: () -> T): T {
        db.beginTransaction(SqlJetTransactionMode.READ_ONLY)
        try {
            return func()
        } finally {
            db.commit()
        }
    }

    private inline fun <T> ISqlJetCursor.use(func: ISqlJetCursor.() -> T): T {
        try {
            return func()
        } finally {
            close()
        }
    }

    private inline fun ISqlJetCursor.forEach(func: () -> Unit) {
        if (!eof()) {
            do {
                func()
            } while (next())
        }
    }

    private inline fun ISqlJetCursor.forEach(count: Int, func: () -> Unit) {
        var x = 0
        if (!eof()) {
            do {
                func()
                x++
            } while (next() && (x < count))
        }
    }

    private fun table(name: String) = db.getTable(name)

    private object LogsTable : BaseTable() {
        override val TableName: String = "logs"
        val Task = Field("task", 0)
        val Message = Field("message", 1)

        val TaskIndex = "taskIndex"
    }

    private object HistoryTable : BaseTable() {
        override val TableName: String = "history"
        val Task = Field("task", 0)
        val Start = Field("start", 1)
        val End = Field("end", 2)
        val Completed = Field("completed", 3)
        val Scheduling = Field("scheduling", 4)

        val TaskIndex = "historyTaskIndex"

    }
}