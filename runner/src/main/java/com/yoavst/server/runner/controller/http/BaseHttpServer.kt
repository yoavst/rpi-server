package com.yoavst.server.runner.controller.http

interface BaseHttpServer {
    fun start()
    fun stop()
}