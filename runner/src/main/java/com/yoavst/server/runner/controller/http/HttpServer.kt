package com.yoavst.server.runner.controller.http

interface HttpServer: BaseHttpServer {
    fun setTokens(tokens: List<String>)
}