package com.yoavst.server.runner.model

import com.yoavst.server.Option
import com.yoavst.server.SubTask
import com.yoavst.server.Task
import com.yoavst.server.TaskResults

class ManagedTask(private val wrappedTask: Task, enabled: Boolean, rule: String, private val notifier: (task: ManagedTask) -> Unit) :
        Task(wrappedTask.id, wrappedTask.name, wrappedTask.description) {
    var enabled: Boolean = enabled
        set(value) {
            field = value
            notifier(this)
        }

    var rule: String = rule
        set(value) {
            field = value
            notifier(this)
        }

    override fun invoke(): TaskResults = wrappedTask.invoke()

    override val settings: List<Option<*>>
        get() = wrappedTask.settings
    override val subTasks: List<SubTask>
        get() = wrappedTask.subTasks
}