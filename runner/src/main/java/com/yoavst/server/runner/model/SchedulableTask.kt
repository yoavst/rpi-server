package com.yoavst.server.runner.model

import com.yoavst.server.Task

class SchedulableTask(val task: Task, val rule: String)