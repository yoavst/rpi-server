package com.yoavst.server.runner.controller.http

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import com.yoavst.server.*
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.runner.AuthError
import com.yoavst.server.runner.HttpPort
import com.yoavst.server.runner.TaskError
import com.yoavst.server.runner.TokenParameter
import com.yoavst.server.runner.controller.Server
import com.yoavst.server.runner.controller.scheduling.SchedulerService
import com.yoavst.server.runner.logging.LoggerFactory
import com.yoavst.server.runner.logging.storage.HistoryStorage
import com.yoavst.server.runner.model.ManagedTask
import com.yoavst.server.runner.util.toDoubleOrNull
import com.yoavst.server.runner.util.toIntOrNull
import com.yoavst.server.runner.util.toLongOrNull
import org.aaronhe.threetengson.ThreeTenGsonAdapter
import org.threeten.bp.Duration
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeParseException
import spark.Request
import spark.Response
import spark.Spark.port

class RestHttpServer(task: Task, val server: Server, private val schedulerServer: SchedulerService,
                     val storage: Storage, private val authEnabled: Boolean) : HttpServer, Logging(task, "HTTP server") {
    private val gson = ThreeTenGsonAdapter.registerAll(GsonBuilder()).setPrettyPrinting().create()
    private fun json(obj: Any) = gson.toJson(obj)
    private val authError = json(Message(AuthError, "Auth error."))

    override fun start() {
        port(HttpPort)
        get("/list", "Show the list of available tasks.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                json(server.tasks.map { ApiTask(it, storage) })
            } else {
                res.type("application/json")
                authError
            }
        }
        get("/:task/enable", "Enable :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        logger.info { "Enabling task with the id '$taskId'" }
                        task.enabled = true
                        server.sync()
                        json(Message.ok("Task with the given id '$taskId' is enabled."))
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }
        get("/:task/disable", "Disable :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        logger.info { "Disabling task with the id '$taskId'" }
                        task.enabled = false
                        server.sync()
                        json(Message.ok("Task with the given id '$taskId' is disabled."))
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }
        get("/:task/run", "Run :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        logger.info { "Running task with the id '$taskId'" }
                        try {
                            task.logger // init the logger
                            val logger = LoggerFactory[task]
                            logger.record()
                            val results = server.run(task)
                            json(ApiTaskResults(results, logger.stopRecording()))
                        } catch (t: Throwable) {
                            logger.error(t) { "Error running task with the given id '$taskId'" }
                            json(Message.error(t.message ?: "Error while running the task"))
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        get("/:task/:subtask/run", "Run :subtask of :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        res.type("application/json")
                        val subtaskId = req.params(":subtask")
                        if (subtaskId == null)
                            json(Message.error("Subtask parameter not supplied."))
                        else {
                            val subtask = task.subTasks.firstOrNull { it.id == subtaskId }
                            if (subtask == null)
                                json(Message.error("Subtask with the given id '$subtaskId' for task '$taskId' does not exist."))
                            else {
                                logger.info { "Running subtask with id '$subtaskId' for task '$taskId'" }
                                try {
                                    val logger = LoggerFactory.getLogger(subtask, subtask.javaClass, subtask.name)
                                    logger.record()
                                    val results = server.run(subtask)
                                    json(ApiTaskResults(results, logger.stopRecording()))
                                } catch (t: Throwable) {
                                    logger.error(t) { "Error running subtask with the given id '$subtaskId' for task '$taskId'" }
                                    json(Message.error(t.message ?: "Error while running the subtask"))
                                }
                            }
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        post("/:task/rule", "Set scheduling rule for :task. POST body is the new rule.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        val body = req.body()
                        if (schedulerServer.isValidRule(body)) {
                            task.rule = body
                            schedulerServer.schedule(body, task)
                        } else {
                            error("Invalid rule for task '$taskId'")
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        post("/:task/option/:option", "Set option :option for :task. POST body is the new value.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    val task = server.tasks.firstOrNull { it.id == taskId }
                    if (task == null)
                        json(Message.error("Task with the given id '$taskId' does not exist."))
                    else {
                        val optionId = req.params(":option")
                        if (optionId == null)
                            json(Message.error("Option parameter not supplied."))
                        else {
                            val option = task.settings.firstOrNull { it.id == optionId }
                            if (option == null)
                                json(Message.error("Option with the given id '$optionId' doesn't exists for task '$taskId'"))
                            else {
                                val body = req.body()
                                if (body.isEmpty()) {
                                    Environment.storage[task, option] = null
                                    json(Message.ok("Cleared option with the given id '$optionId' for task '$taskId'"))
                                } else {
                                    var data: Any? = null
                                    when (option) {
                                        is Option.Integer -> {
                                            data = body.toIntOrNull()
                                        }
                                        is Option.Decimal -> {
                                            data = body.toDoubleOrNull()
                                        }
                                        is Option.Bool -> {
                                            data = body.toBoolean()
                                        }
                                        is Option.Str -> {
                                            data = body
                                        }
                                        is Option.Date -> {
                                            try {
                                                data = gson.fromJson<LocalDate>(body)
                                            } catch (e: DateTimeParseException) {
                                                data = null
                                            }
                                        }
                                        is Option.Duration -> {
                                            val millis = body.toLongOrNull()
                                            if (millis != null) {
                                                data = Duration.ofMillis(millis)
                                            }
                                        }
                                    }
                                    if (data == null)
                                        json(Message.error("Unsupported data type. Supported: ${option.javaClass.simpleName}"))
                                    else {
                                        Environment.storage[task, option] = data
                                        val message = "Task: '$taskId', Option: '$optionId', new value: '$data'"
                                        logger.info { message }
                                        json(Message.ok(message))
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        get("/logs/clear/:task", "Clear logs for :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                if (taskId == null)
                    json(Message.error("Task parameter not supplied."))
                else {
                    if (taskId == "all") {
                        HistoryStorage.clearAllLogs()
                        json(Message.ok("Cleaned logs for all tasks"))
                    } else if (taskId == "runner") {
                        HistoryStorage.clearLogsFor("runner")
                        json(Message.ok("Cleaned logs for the runner."))
                    } else {
                        val task = server.tasks.firstOrNull { it.id == taskId }
                        if (task == null)
                            json(Message.error("Task with the given id '$taskId' does not exist."))
                        else {
                            HistoryStorage.clearLogsFor(task)
                            json(Message.ok("Cleaned logs for task '$taskId'"))
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }
        get("/logs/:task/:count", "Show last :count logs for :task.") { req, res ->
            if (validate(req)) {
                res.type("text/plain")
                val taskId = req.params(":task")
                val count = req.params(":count")?.toIntOrNull()
                if (taskId == null || count == null)
                    json(Message.error("Task or count parameters not supplied."))
                else {
                    if (taskId == "all") {
                        HistoryStorage.getLogs(count).joinToString("\n")
                    } else if (taskId == "runner") {
                        HistoryStorage.getLogs(count, "runner").joinToString("\n")
                    } else {
                        val task = server.tasks.firstOrNull { it.id == taskId }
                        if (task == null)
                            json(Message.error("Task with the given id '$taskId' does not exist."))
                        else {
                            HistoryStorage.getLogs(count, task).joinToString("\n")
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        get("/history/:task/:count", "Show the last :count history rows of :task.") { req, res ->
            if (validate(req)) {
                res.type("application/json")
                val taskId = req.params(":task")
                val count = req.params(":count")?.toIntOrNull()
                if (taskId == null || count == null)
                    json(Message.error("Task or count parameters not supplied."))
                else {
                    if (taskId == "all") {
                        json(HistoryStorage.getHistory(count))
                    } else {
                        val task = server.tasks.firstOrNull { it.id == taskId }
                        if (task == null)
                            json(Message.error("Task with the given id '$taskId' does not exist."))
                        else {
                            json(HistoryStorage.getHistory(count, task))
                        }
                    }
                }
            } else {
                res.type("application/json")
                authError
            }
        }

        get("/sync", "Synchronize the data.") { req, res ->
            if (validate(req)) {
                server.sync()
            } else {
                res.type("application/json")
                authError
            }
        }

        get("/help", "Show help.") { _, res ->
            res.type("text/plain")
            commands.toList().joinToString("\n") { it.first + " - " + it.second }
        }
    }

    override fun stop() {
        spark.Spark.stop()
    }

    private var tokens: List<String> = listOf()
    override fun setTokens(tokens: List<String>) {
        this.tokens = tokens
    }

    private fun validate(req: Request): Boolean {
        return if (authEnabled) {
            val result = req.queryParams(TokenParameter) in tokens
            if (!result) {
                logger.warn { "User tried to use API without valid token. IP: ${req.ip()}. Host: ${req.host()}" }
            }
            result
        } else true
    }


    data class ApiTask(val id: String, val name: String, val description: String, val settings: List<ApiOption<*>>,
                       val subTasks: List<ApiSubtask>, val enabled: Boolean, val rule: String) {
        constructor(task: ManagedTask, storage: Storage) : this(task.id, task.name, task.description, task.settings.map {
            ApiOption(task, it, storage)
        }, task.subTasks.map(::ApiSubtask), task.enabled, task.rule)
    }

    data class ApiSubtask(val id: String, val name: String, val description: String) {
        constructor(task: SubTask) : this(task.id, task.name, task.description)
    }

    @Suppress("UNCHECKED_CAST")
    data class ApiOption<out K : Any>(val id: String, val name: String, val description: String, val defaultValue: K, val options: List<K>?,
                                      val isProtected: Boolean = false, val currentValue: K) {
        constructor(task: Task, option: Option<K>, storage: Storage) : this(option.id, option.name, option.description, option.defaultValue, option.options,
                if (option is Option.Str) option.isProtected else false, storage[task, option].let {
            if (option is Option.Str && option.isProtected) (it as String).map { '*' }.joinToString("") as K
            else it
        })
    }

    data class Message(val status: Int, val message: String) {
        companion object {
            fun ok(text: String) = Message(0, text)
            fun error(text: String) = Message(TaskError, text)
        }
    }

    data class ApiTaskResults(val taskResults: TaskResults, val logging: List<String>)

    companion object {
        private const val BasePath = "/server"
        val commands: MutableMap<String, String> = mutableMapOf()

        fun get(path: String, description: String, route: (req: Request, res: Response) -> Any?) {
            val realPath = BasePath + path
            commands["GET $realPath"] = description
            spark.Spark.get(realPath, route)
        }

        fun post(path: String, description: String, route: (req: Request, res: Response) -> Any?) {
            val realPath = BasePath + path
            commands["POST $realPath"] = description
            spark.Spark.post(realPath, route)
        }
    }
}