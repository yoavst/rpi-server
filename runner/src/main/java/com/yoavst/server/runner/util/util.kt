package com.yoavst.server.runner.util

import java.io.File

operator fun File.div(name: String): File {
    return File(this, name)
}

fun File?.listFilesRecursively(): List<File> {
    val fileTree = mutableListOf<File>()
    if (this == null)
        return fileTree

    for (entry in listFiles()) {
        if (entry.isFile)
            fileTree += entry
        else
            fileTree.addAll(entry.listFilesRecursively())
    }
    return fileTree
}

inline fun <T> Iterable<T>.peek(f: (T) -> Unit): Iterable<T> = apply { forEach(f) }
