package com.yoavst.server.runner.controller.scheduling

import com.yoavst.server.Invokable
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.TaskResults.Scheduling.Retry
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.runner.util.fullFormat
import com.yoavst.server.runner.util.toTime
import it.sauronsoftware.cron4j.Predictor
import it.sauronsoftware.cron4j.Scheduler
import it.sauronsoftware.cron4j.SchedulingPattern
import org.threeten.bp.Instant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class CRON4JSchedulerService(task: Task, override var taskInterceptor: (Task) -> TaskResults = Task::invoke) :
        SchedulerService, Logging(task, "Scheduler Service") {
    private var scheduler: Scheduler = Scheduler()
    private val rescheduler: ScheduledExecutorService = Executors.newScheduledThreadPool(1)

    private val scheduledTasks: MutableMap<String, String> = mutableMapOf()
    private val rescheduledTasks: MutableMap<Task, Int> = mutableMapOf()


    override fun start() {
        scheduler.start()
    }

    override fun stop() {
        scheduler.stop()
    }

    override fun schedule(rule: String, task: Task) {
        if (task.id in scheduledTasks) {
            scheduler.reschedule(scheduledTasks[task.id], rule)
            logger.info { "Rescheduling task '${task.id}' with rule '$rule'" }
        } else {
            scheduledTasks[task.id] = scheduler.schedule(rule, taskFor(task))
            logger.info { "Scheduling task '${task.id}' with rule '$rule'" }
        }
        logger.info { "Task '${task.id}' will run at ${nextDateFor(rule).fullFormat()}" }
    }

    override fun stopSchedule(id: String) {
        val schedulingId = scheduledTasks[id]
        if (schedulingId == null) {
            // Ignored
        } else {
            scheduler.deschedule(schedulingId)
            logger.info { "successfully stop scheduling task '$id'." }
        }
    }

    fun taskFor(task: Task) = object : Runnable {
        override fun run() {
            val id = scheduledTasks[task.id]
            val results = taskInterceptor(task)
            val scheduling = results.scheduling
            if (scheduling is Retry) {
                val duration = scheduling.duration
                logger.debug { "Task '${task.id}' requested for retry in ${duration.toMillis()} ms." }
                val next = nextDateFor(scheduler.getSchedulingPattern(id)).toTime()
                val rescheduleDate = Instant.now() + duration
                logger.debug {
                    "Task '${task.id}' next scheduling: ${next.fullFormat()}. Requested retry: ${rescheduleDate.fullFormat()}"
                }
                if (rescheduleDate > next) {
                    rescheduledTasks.remove(task)
                    logger.debug { "Task '${task.id}' retry will be after next scheduled time. Ignoring request." }
                } else {
                    val data = rescheduledTasks[task] ?: 0
                    if (data < ReschedulingCount) {
                        logger.debug { "Task '${task.id}' will be rescheduled for the ${data + 1} time" }
                        rescheduler.schedule(this, duration.toMillis(), TimeUnit.MILLISECONDS)
                        rescheduledTasks[task] = data + 1
                    } else {
                        logger.debug { "Task '${task.id}' was already rescheduled for $ReschedulingCount times. Stopping." }
                        rescheduledTasks.remove(task)
                    }
                }
            } else {
                rescheduledTasks.remove(task)
                logger.debug { "Task '${task.id}' will run again at ${nextDateFor(scheduler.getSchedulingPattern(id)).fullFormat()}" }
            }
        }
    }

    override fun isValidRule(rule: String): Boolean = SchedulingPattern.validate(rule)

    companion object {
        const val ReschedulingCount = 2

        private fun nextDateFor(pattern: String) = Predictor(pattern).nextMatchingTime()
        private fun nextDateFor(pattern: SchedulingPattern) = Predictor(pattern).nextMatchingTime()
    }
}