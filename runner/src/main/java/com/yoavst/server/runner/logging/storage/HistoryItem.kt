package com.yoavst.server.runner.logging.storage

data class HistoryItem(val taskId: String, val startTime: Long, val endTime: Long = 0, val isCompleted: Boolean = false, val scheduling: Long = NoScheduling) {
    companion object {
        val NoScheduling: Long = 0
    }
}