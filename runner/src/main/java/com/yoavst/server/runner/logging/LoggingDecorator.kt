package com.yoavst.server.runner.logging

interface LoggingDecorator {
    fun decorate(message: String, level: Int, clazz: String, name: String, time: Long): String

    fun decorate(t: Throwable, message: String, level: Int, clazz: String, name: String, time: Long): String
}