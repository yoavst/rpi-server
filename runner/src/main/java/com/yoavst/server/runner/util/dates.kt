package com.yoavst.server.runner.util

import org.threeten.bp.Instant
import java.text.SimpleDateFormat
import java.util.*

val format = SimpleDateFormat("dd/MM HH:mm:ss")

fun Instant.fullFormat(): String = millis.fullFormat()
fun Long.fullFormat(): String = toDate().fullFormat()
fun Date.fullFormat(): String = format.format(this)

fun Long.toTime(): Instant = Instant.ofEpochMilli(this)
fun Long.toDate(): Date = Date(this)

val Instant.millis: Long
    get() = toEpochMilli()