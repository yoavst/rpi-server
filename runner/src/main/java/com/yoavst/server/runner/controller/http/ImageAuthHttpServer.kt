package com.yoavst.server.runner.controller.http

import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.runner.util.ImageEncoder
import com.yoavst.server.runner.controller.http.RestHttpServer.Companion.get
import java.io.File
import java.math.BigInteger
import java.security.SecureRandom

class ImageAuthHttpServer(val task: Task, val tokenFile: File) : AuthHttpServer {
    val image = Environment.storage.file(OriginalImage)
    val servedFile = Environment.storage.file(task, ServedImage)
    val keyFile = Environment.storage.file(task, KeyFilename)

    private val random = SecureRandom()

    override fun start() {
        servedFile.delete()
        val key = keyFile.readText()
        if (key.isEmpty()) {
            generateNewPassword()
        } else {
            generateImage(key)
        }

        get("/auth", "Authentication for the server.") { _, res ->
            res.type("image/jpeg")
            servedFile.inputStream().use {
                it.copyTo(res.raw().outputStream)
            }

        }

        get("/tokenize", "Generate new token.") { _, res ->
            generateNewPassword()
            res.type("text/plain")
            "OK"
        }
    }

    override fun stop() {
        spark.Spark.stop()
    }

    override fun generateNewPassword() {
        val key = nextSessionId()
        keyFile.writeText(key)
        tokenFile.writeText(key)
        generateImage(key)
    }

    private fun generateImage(key: String) {
        ImageEncoder.encodeText(image, servedFile.apply { createNewFile() }, key)
    }


    fun nextSessionId(): String {
        return BigInteger(130, random).toString(16)
    }


    companion object {
        const val KeyFilename ="key.txt"
        const val OriginalImage ="original.jpg"
        const val ServedImage ="served.jpg"

    }
}