package com.yoavst.server.runner.logging.impl

import com.yoavst.server.runner.logging.RecordingLogger

class ChildLogger(val parentLogger: RecordingLogger, val clazz: String, val name: String): RecordingLogger by parentLogger {
    override fun log(level: Int, message: Any?) {
        logPlain(decorate(message.toString(), level, clazz, name, System.currentTimeMillis()))
    }

    override fun log(level: Int, throwable: Throwable, message: Any?) {
        logPlain(decorate(throwable, message.toString(), level, clazz, name, System.currentTimeMillis()))
    }
}