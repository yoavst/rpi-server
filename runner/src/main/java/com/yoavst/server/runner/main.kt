package com.yoavst.server.runner

import com.beust.jcommander.JCommander
import com.yoavst.server.Environment
import com.yoavst.server.runner.controller.RunnerTask
import com.yoavst.server.runner.controller.storage.MainStorage
import com.yoavst.server.runner.logging.LoggerFactory
import com.yoavst.server.runner.logging.impl.NoLogging
import com.yoavst.server.runner.util.CommandLineParams
import java.io.File
import java.util.logging.LogManager
import java.util.logging.Logger


fun main(args: Array<String>) {
    logging()

    val params = CommandLineParams()
    val jCommander = JCommander(params, *args)
    jCommander.setProgramName(className)
    if (params.help) {
        jCommander.usage()
    } else {
        val folder = File(params.dataFolder)
        val runnerTask = RunnerTask(params)
        Environment.storage = MainStorage(runnerTask, folder)

        runnerTask()
    }
}

fun logging() {
    Environment.loggerFactory = { task, name, clazz -> LoggerFactory.getLogger(task, clazz, name) }

    org.eclipse.jetty.util.log.Log.setLog(NoLogging())
    LogManager.getLogManager().reset()
    Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME).level = java.util.logging.Level.OFF

    val properties = System.getProperties()
    properties["$slf4j.logFile"] = "System.err" //TODO change to file
    properties["$slf4j.defaultLogLevel"] = "debug"
    properties["$slf4j.showDateTime"] = "true"
    properties["$slf4j.dateTimeFormat"] = "dd/MM HH:mm:ss"
    properties["$slf4j.showShortLogName"] = "true"
    properties["$slf4j.levelInBrackets"] = "true"
    properties["$slf4j.showThreadName"] = "false"
    properties["$slf4j.log.spark"] = "info"
}


const val slf4j = "org.slf4j.simpleLogger"