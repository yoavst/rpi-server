package com.yoavst.server.runner.controller

import com.yoavst.server.Environment
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.runner.controller.http.AuthHttpServer
import com.yoavst.server.runner.controller.http.HttpServer
import com.yoavst.server.runner.controller.http.ImageAuthHttpServer
import com.yoavst.server.runner.controller.http.RestHttpServer
import com.yoavst.server.runner.controller.scheduling.CRON4JSchedulerService
import com.yoavst.server.runner.controller.scheduling.SchedulerService
import com.yoavst.server.runner.util.CommandLineParams
import com.yoavst.server.runner.util.FileWatchdog
import java.io.File


class RunnerTask(val params: CommandLineParams) : Task(
        id = "runner",
        name = "Runner",
        description = "The runner of the server"
) {
    override fun invoke(): TaskResults {
        val storage = Environment.storage

        val schedulerService: SchedulerService = CRON4JSchedulerService(this)
        val server: Server = ServerImpl(this, storage, schedulerService)

        server.start()

        val httpServer: HttpServer = RestHttpServer(this, server, schedulerService, storage, params.auth)

        if (!update(params.tokensFile, httpServer)) {
            update(File(params.dataFolder, params.tokensFile).absolutePath, httpServer)
        }
        return TaskResults.success()
    }

    private fun update(tokensFile: String, httpServer: HttpServer): Boolean {
        if (File(tokensFile).exists()) {
            object : FileWatchdog(tokensFile) {
                override fun doOnChange() {
                    updateTokens(tokensFile, httpServer)
                }
            }

            updateTokens(tokensFile, httpServer)
            httpServer.start()

            val authHttpServer: AuthHttpServer = ImageAuthHttpServer(this, File(tokensFile))
            authHttpServer.start()
            return true
        }
        return false
    }

    private fun updateTokens(tokensFile: String, server: HttpServer) {
        server.setTokens(File(tokensFile).readLines())
    }
}