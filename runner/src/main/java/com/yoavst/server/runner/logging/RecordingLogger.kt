package com.yoavst.server.runner.logging

import com.yoavst.server.logging.RawLogger

interface RecordingLogger: PlainLogger {
    fun record()
    fun stopRecording(): List<String>
}