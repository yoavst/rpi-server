package com.yoavst.server.runner.util

import com.beust.jcommander.Parameter

class CommandLineParams(
        @Parameter(names = arrayOf("--path", "-p"), description = "Path for configuration and storage")
        val dataFolder: String = "/var/lib/yoavServer",
        @Parameter(names = kotlin.arrayOf("--token", "-t"), description = "Path for token text file. Each line is a valid token")
        val tokensFile: String = "tokens.txt",
        @Parameter(names = kotlin.arrayOf("--auth", "-a"), description = "Enable authentication for requests")
        val auth: Boolean = false,
        @Parameter(names = arrayOf("--help", "-h"), description = "Show usage info", help = true)
        val help: Boolean = false

)