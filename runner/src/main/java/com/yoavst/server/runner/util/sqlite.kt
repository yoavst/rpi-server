package com.yoavst.server.runner.util

data class Field(val name: String, val column: Int)

abstract class BaseTable {
    abstract val TableName: String
    val Id = Field("_ID", 0)
}

/**
 * Appends an SQL string to the given StringBuilder, including the opening
 * and closing single quotes. Any single quotes internal to sqlString will
 * be escaped.

 * @param sb the StringBuilder that the SQL string will be appended to
 * *
 * @param sqlString the raw string to be appended, which may contain single
 * *                  quotes
 */
fun appendEscapedSQLString(sb: StringBuilder, sqlString: String) {
    if (sqlString.indexOf('\'') != -1) {
        val length = sqlString.length
        for (i in 0..length - 1) {
            val c = sqlString[i]
            if (c == '\'') {
                sb.append('\'')
            }
            sb.append(c)
        }
    } else
        sb.append(sqlString)
}

/**
 * SQL-escape a string.
 */
fun sqlEscapeString(value: String): String {
    val escaper = StringBuilder()

    appendEscapedSQLString(escaper, value)

    return escaper.toString()
}
