package com.yoavst.server.runner.controller.storage

import com.yoavst.server.Option
import com.yoavst.server.Storage
import com.yoavst.server.Task
import com.yoavst.server.logging.impl.Logging
import com.yoavst.server.runner.PrefsName
import com.yoavst.server.runner.modulesDataFolder
import com.yoavst.server.runner.modulesFolder
import com.yoavst.server.runner.util.div
import com.yoavst.server.runner.util.listFilesRecursively
import java.io.File
import java.util.*

class MainStorage(task: Task, private val dataFolder: File) : Storage, Logging(task, "Storage") {
    private val prefsCache: MutableMap<String, Preferences> = HashMap()

    override fun file(name: String): File = dataFolder / name
    override fun file(task: Task, name: String, create: Boolean): File = file(task.id, name, create)
    override fun folder(task: Task): File = folder(task.id)

    override fun mainFile(name: String, create: Boolean): File = file("runner", name, create)

    private fun file(folder: String, name: String, create: Boolean): File {
        return (folder(folder) / name).create(create)
    }

    private fun folder(folder: String): File {
        return (dataFolder / modulesDataFolder / folder).apply { mkdirs() }
    }

    override val modules: List<File>
        get() = (dataFolder / modulesFolder).listFilesRecursively().filter { it.extension == "jar" }

    @Suppress("UNCHECKED_CAST")
    override fun <K : Any> get(task: Task, option: Option<K>): K {
        val prefs = prefs(task)
        when (option) {
            is Option.Integer -> {
                return (prefs.getInteger(option.id) ?: option.defaultValue) as K
            }
            is Option.Decimal -> {
                return (prefs.getDecimal(option.id) ?: option.defaultValue) as K
            }
            is Option.Bool -> {
                return (prefs.getBool(option.id) ?: option.defaultValue) as K
            }
            is Option.Str -> {
                return (prefs.getString(option.id) ?: option.defaultValue) as K
            }
            is Option.Date -> {
                return (prefs.getDate(option.id) ?: option.defaultValue) as K
            }
            is Option.Duration -> {
                return (prefs.getDuration(option.id) ?: option.defaultValue) as K
            }
        }
    }

    override fun <K : Any> set(task: Task, option: Option<K>, value: K?) {
        prefs(task)[option.id] = value
        logger.debug { "${task.id}[${option.id}] = $value" }
    }

    private fun prefs(task: Task) = prefsCache.getOrPut(task.id) { JsonPreferences(prefsFile(task)) }
    private fun prefsFile(task: Task) = file(task, PrefsName)

    private fun File.create(create: Boolean = true): File {
        if (create) {
            parentFile.mkdirs()
            createNewFile()
        }
        return this
    }
}
