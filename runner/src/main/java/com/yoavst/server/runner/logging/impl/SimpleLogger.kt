package com.yoavst.server.runner.logging.impl

import com.yoavst.server.Task
import com.yoavst.server.runner.ShouldPrintToSystemOut
import com.yoavst.server.runner.logging.LoggingDecorator
import com.yoavst.server.runner.logging.RecordingLogger
import com.yoavst.server.runner.logging.storage.HistoryStorage


class SimpleLogger(override val level: Int, val decorator: LoggingDecorator, val task: Task, val clazz: String, val name: String) :
        RecordingLogger, LoggingDecorator by decorator {
    private var isRecording: Boolean = false
    private var recordings: MutableList<String> = mutableListOf()


    override fun log(level: Int, message: Any?) {
        if (level >= this.level)
            logPlain(decorator.decorate(message.toString(), level, clazz, name, System.currentTimeMillis()))
    }

    override fun log(level: Int, throwable: Throwable, message: Any?) {
        if (level >= this.level)
            logPlain(decorator.decorate(throwable, message.toString(), level, clazz, name, System.currentTimeMillis()))
    }

    override fun logPlain(message: String) {
        if (isRecording)
            recordings.add(message)

        if (ShouldPrintToSystemOut)
            System.err.println(message)

        HistoryStorage.log(task, message)
    }

    override fun record() {
        isRecording = true
    }

    override fun stopRecording(): List<String> {
        isRecording = false
        return recordings.apply { recordings = mutableListOf() }
    }
}