package com.yoavst.server.runner.controller

import com.yoavst.server.SubTask
import com.yoavst.server.Task
import com.yoavst.server.TaskResults
import com.yoavst.server.runner.model.ManagedTask
import java.io.File

interface Server {
    //region Tasks
    val tasks: List<ManagedTask>
    fun loadModule(file: File): Boolean

    fun sync()
    //endregion

    fun start()

    fun run(task: Task): TaskResults
    fun run(task: SubTask): TaskResults

}