package com.yoavst.server.runner.util;

import kotlin.jvm.functions.Function1;
import kotlin.text.Charsets;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public class ImageEncoder {
    private static final Charset charset = Charsets.US_ASCII;

    private static byte[] toBitArray(byte[] arr) {
        byte[] bitArray = new byte[arr.length * 8];
        for (int i = 0; i < arr.length; i++) {
            byte b = arr[i];
            for (int p = 0; p < 8; p++) {
                bitArray[i * 8 + p] = (byte) (getBit(b, p));
            }
        }
        return bitArray;
    }

    private static byte[] toByteArray(byte[] arr) {
        byte[] byteArray = new byte[arr.length / 8];
        try {
            for (int i = 0; i < arr.length; i++) {
                byte b = 0;
                for (int p = 0; p < 8; p++) {
                    b = setBit(b, p, arr[i*8+p]);
                }
                byteArray[i] = b;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {}
        return byteArray;
    }

    public static void encodeText(File input, File output, String text) {
        try {
            BufferedImage img = ImageIO.read(input);
            byte[] data = toBitArray(text.getBytes(charset));
            int p = 0;
            loop:
            for (int w = 0; w < img.getWidth(); w++) {
                for (int h = 0; h < img.getHeight(); h++) {
                    if (p >= data.length) {
                        break loop;
                    }
                    int realRGB = img.getRGB(w, h);
                    byte b = data[p];
                    if (b == 1) {
                        realRGB |= 1;
                    } else {
                        realRGB &= ~0b1;
                    }
                    img.setRGB(w, h, realRGB);
                    p++;
                }
            }
            ImageIO.write(img, "bmp", output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String decodeText(File output, Function1<Character, Boolean> stopper) {
        try {
            BufferedImage img = ImageIO.read(output);
            byte[] bitArray = new byte[8];
            int i = 0;
            StringBuilder stringBuilder = new StringBuilder();
            for (int w = 0; w < img.getWidth(); w++) {
                for (int h = 0; h < img.getHeight(); h++) {
                    int pixel = img.getRGB(w, h);
                    byte value = (byte) (pixel & 1);
                    bitArray[i] = value;
                    i++;
                    if (i == 8) {
                        i = 0;
                        char c = new String(toByteArray(bitArray)).charAt(0);
                        if (stopper.invoke(c)) {
                            return stringBuilder.toString();
                        } else {
                            stringBuilder.append(c);
                        }
                    }
                }
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static int getBit(byte b, int position) {
        return ((b >> position) & 1);
    }

    private static byte setBit(byte b, int pos, int value) {
        if (value == 1) {
            return (byte) (b | (1 << pos));
        } else {
            return (byte) (b & ~(1 << pos));
        }
    }
}