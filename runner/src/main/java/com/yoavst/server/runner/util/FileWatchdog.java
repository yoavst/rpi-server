/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Contributors:  Mathias Bogaert

package com.yoavst.server.runner.util;

import java.io.File;

/**
 * Check every now and then that a certain file has not changed. If it
 * has, then call the {@link #doOnChange} method.
 *
 * @author Ceki G&uuml;lc&uuml;
 * @since version 0.9.1
 */
public abstract class FileWatchdog extends Thread {

    /**
     * The default delay between every file modification check, set to 60
     * seconds.
     */
    private static final long DEFAULT_DELAY = 60000;

    private File file;
    private long lastModify = 0;
    private boolean warnedAlready = false;
    private boolean interrupted = false;

    protected FileWatchdog(String filename) {
        super("FileWatchdog");
        /*
      The name of the file to observe  for changes.
     */
        file = new File(filename);
        setDaemon(true);
        checkAndConfigure();
    }

    abstract protected void doOnChange();

    private void checkAndConfigure() {
        boolean fileExists;
        try {
            fileExists = file.exists();
        } catch (SecurityException e) {
            interrupted = true; // there is no point in continuing
            return;
        }

        if (fileExists) {
            long l = file.lastModified(); // this can also throw a SecurityException
            if (l > lastModify) {           // however, if we reached this point this
                lastModify = l;              // is very unlikely.
                doOnChange();
                warnedAlready = false;
            }
        } else {
            if (!warnedAlready) {
                warnedAlready = true;
            }
        }
    }

    public void run() {
        while (!interrupted) {
            try {
                Thread.sleep(DEFAULT_DELAY);
            } catch (InterruptedException e) {
                // no interruption expected
            }
            checkAndConfigure();
        }
    }
}