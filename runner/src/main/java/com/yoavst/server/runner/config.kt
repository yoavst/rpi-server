package com.yoavst.server.runner

const val className: String = "yoav_server_v1.jar"

const val TaskClassNameInManifest = "task"
const val TaskDefaultSchedulingRuleInManifest = "scheduling_rule"

const val DatabaseName: String = "history.db"

const val modulesFolder: String = "modules"
const val modulesDataFolder: String = "data"

const val PrefsName: String = "preferences.json"

const val TaskError: Int = -1
const val AuthError: Int = -2

const val HttpPort: Int = 3000

const val ShouldPrintToSystemOut: Boolean = true

const val TokenParameter: String = "token"

