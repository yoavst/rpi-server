package com.yoavst.server.runner.controller.scheduling

import com.yoavst.server.Invokable
import com.yoavst.server.Task
import com.yoavst.server.TaskResults

interface SchedulerService {
    fun start()
    fun stop()

    fun schedule(rule: String, task: Task)

    fun stopSchedule(task: Invokable) = stopSchedule(task.id)
    fun stopSchedule(id: String)

    fun isValidRule(rule: String): Boolean

    var taskInterceptor: (Task) -> TaskResults
}