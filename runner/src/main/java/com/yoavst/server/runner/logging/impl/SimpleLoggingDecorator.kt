package com.yoavst.server.runner.logging.impl

import com.yoavst.server.runner.logging.LoggingDecorator
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*

object SimpleLoggingDecorator : LoggingDecorator {
    override fun decorate(message: String, level: Int, clazz: String, name: String, time: Long): String {
        return "${timeFormat.format(Date(time))} [${LogLevel[level]}] $name - $message"
    }

    override fun decorate(t: Throwable, message: String, level: Int, clazz: String, name: String, time: Long): String {
        return decorate(message, level, clazz, name, time) + "\n" + getStackTrace(t)
    }

    @JvmStatic
    val LogLevel: Array<String> = arrayOf("", "Debug", "Info", "Warn", "Error")
    @JvmStatic
    val timeFormat: SimpleDateFormat = SimpleDateFormat("dd/MM HH:mm:ss")

    fun getStackTrace(t: Throwable): String {
        val result = StringWriter()
        return PrintWriter(result).use {
            t.printStackTrace(it)
            result.toString()
        }
    }
}