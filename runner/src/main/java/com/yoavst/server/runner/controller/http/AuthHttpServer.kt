package com.yoavst.server.runner.controller.http

interface AuthHttpServer: BaseHttpServer {
    fun generateNewPassword()
}