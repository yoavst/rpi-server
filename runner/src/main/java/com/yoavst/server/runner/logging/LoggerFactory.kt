package com.yoavst.server.runner.logging

import com.yoavst.server.SubTask
import com.yoavst.server.Task
import com.yoavst.server.logging.RawLogger
import com.yoavst.server.runner.logging.impl.ChildLogger
import com.yoavst.server.runner.logging.impl.SimpleLogger
import com.yoavst.server.runner.logging.impl.SimpleLoggingDecorator

object LoggerFactory {
    val level: Int = RawLogger.Info

    private val loggers: MutableMap<Task, RecordingLogger> = mutableMapOf()
    private val levels: MutableMap<String, Int> = mutableMapOf()

    operator fun get(task: Task): RecordingLogger = loggers[task]!!
    operator fun set(taskId: String, logLevel: Int) = levels.set(taskId, logLevel)

    fun getLogger(task: Task, clazz: Class<*>, name: String): RecordingLogger {
        val cachedLogger = loggers.getOrPut(task) {
            SimpleLogger(levels[task.id] ?: level, SimpleLoggingDecorator, task, clazz.simpleName, name)
        }
        if (clazz == task.javaClass) return cachedLogger
        else {
            return ChildLogger(cachedLogger, clazz.simpleName, name)
        }
    }

    fun getLogger(task: SubTask, clazz: Class<*>, name: String): RecordingLogger {
        val cachedLogger = loggers.getOrPut(task) {
            SimpleLogger(levels[task.id] ?: levels[task.task.id] ?: level, SimpleLoggingDecorator, task, clazz.simpleName, name)
        }
        return ChildLogger(cachedLogger, clazz.simpleName, name)
    }
}