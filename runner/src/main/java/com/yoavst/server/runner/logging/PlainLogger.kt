package com.yoavst.server.runner.logging

import com.yoavst.server.logging.RawLogger

interface PlainLogger : RawLogger, LoggingDecorator {
    fun logPlain(message: String)
}