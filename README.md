YoavServer
----------

Server for scheduling tasks with REST api with authentication.


# How to Install
1. Compile the project using `all.bat`. It will generate a folder called `output`:
```
09/30/2016  14:28    <DIR>          .
09/30/2016  14:28    <DIR>          ..
09/20/2016  19:03            34,816 empty_history.db
09/30/2016  14:27    <DIR>          modules
09/25/2016  21:02            40,085 original.jpg
09/30/2016  14:27                54 run.sh
09/30/2016  14:27                54 run.bat
09/30/2016  14:28                12 tokens.txt
09/30/2016  13:53         4,986,865 yoav_server_v1.jar
               5 File(s)      5,061,832 bytes
```
2. Copy all the files from the folder to the server. 
3. Add execute permission to `run.sh`.
```bash
chmod +x run.sh
```
4. Run the script for the first time.
```bash
# Run help
./run.sh --help
# Launch with default folder: /var/lib/yoavServer
./run.sh
# Launch with another folder: ~/server 
./run.sh -p ~/server -t ~/server/tokens.txt
```
5. Add CRON job for launching the server upon reboot.
```
sudo crontab -e
# add line: (replace the path and add parameters if needed)
@reboot /var/lib/yoavServer/run.sh &
# save.
```
6. Install missing SSL certificates: http://stackoverflow.com/a/38312489/4874829